package eu.europa.ec.jrc.lca.commons.security.encryption;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public final class KeysGenerator {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(KeysGenerator.class);

	private static final String ALGORITHM = "RSA";
	
	private KeysGenerator(){}
	
	public static KeyPair getKey(KeyLength length) {
		KeyPairGenerator kpg;
		try {
			kpg = KeyPairGenerator.getInstance(ALGORITHM);
			kpg.initialize(length.getSize());
			return kpg.generateKeyPair();
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error("[getKey]",e);
		}
		return null;
	}

	public static RSAPublicKeySpec getPublicKey(KeyPair kp) {
		try {
			return KeyFactory.getInstance(ALGORITHM).getKeySpec(kp.getPublic(),
					RSAPublicKeySpec.class);
		} catch (InvalidKeySpecException e) {
			LOGGER.error("[getPublicKey]",e);
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error("[getPublicKey]",e);
		}
		return null;
	}

	public static RSAPrivateKeySpec getPrivateKey(KeyPair kp) {
		try {
			return KeyFactory.getInstance(ALGORITHM).getKeySpec(kp.getPrivate(),
					RSAPrivateKeySpec.class);
		} catch (InvalidKeySpecException e) {
			LOGGER.error("[getPrivateKey]",e);
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error("[getPrivateKey]",e);
		}
		return null;
	}
	
	public static byte[] getCipherKey(RSAPrivateKeySpec privateKey) {
		String exp = new String(privateKey.getPrivateExponent().toByteArray());
		String mod = new String(privateKey.getModulus().toByteArray());
		
		int iterations = 1000;
		
		char[] expChars = exp.toCharArray();
		char[] modChars = mod.toCharArray();
		
		byte[] salt;
		try {
			salt = getSalt();
		
			char[] chars = ArrayUtils.addAll(modChars, expChars);
			
			PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 128);
			SecretKeyFactory skf;
			skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			
			byte[] hash;
			hash = skf.generateSecret(spec).getEncoded();
		
			return hash;
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error("[getCipherKey]",e);
		} catch (InvalidKeySpecException e) {
			LOGGER.error("[getCipherKey]",e);
		}
		return null;
	}
	
	private static byte[] getSalt() throws NoSuchAlgorithmException {
		SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
		
		byte[] salt = new byte[16];
		
		random.nextBytes(salt);
		
		return salt;
	}
}
