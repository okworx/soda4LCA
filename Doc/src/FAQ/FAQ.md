![ ](images/soda4LCA_logo_sm.png)

#soda4LCA Frequently Asked Questions (FAQ)


What is soda4LCA?
=================

soda4LCA is a web-based database application designed to store and
retrieve Life Cycle Inventory (LCI) and Life Cycle Impact Assessment
(LCIA) datasets formatted in the ILCD format. It also exposed a RESTful
service interface to communicate directly with other LCA software tools
and/or databases. Multiple soda4LCA nodes can be joined to a network,
where a search operation will query all nodes in the network.

Do I have to purchase a license?
================================

No. soda4LCA is provided in source and binary form under the [GNU
Affero General Public License (AGPL)](http://www.gnu.org/licenses/)
that gives you legal permission to run, copy, distribute and/or modify 
the software.

Does soda4LCA include any LCI/LCIA data?
========================================

No. soda4LCA is merely a sofware application. One source where you may
obtain ILCD datasets free of charge is the European Commission's
Platform on LCA at <http://eplca.jrc.ec.europa.eu/>.

What data formats are supported?
================================

Currently, the European Commission's International Reference Life Cycle
Data Format (ILCD Format) version 1.1 is supported.

What do I need to run a soda4LCA database node?
===============================================

You need a computer running an operating system that supports both Java
and MySQL. Usually that will be Linux, Mac OS X or Windows. Java
(version 1.6 or newer), a MySQL database (version 5.0 or newer) and
Tomcat (version 6.0 or newer) need to be installed on that machine.

I want to join multiple nodes to a network. Do I need to setup a registry?
==========================================================================

To join multiple soda4LCA nodes to a network, a registry is not
necessarily needed. For every node that is supposed to be searching
other nodes, follow these steps:

1.  Log in to the administration interface.

2.  From the "Network" menu, select "Add Node".

3.  Enter the service URL of the node you want to add to the list of
    network nodes.

4.  Repeat step 2 and 3 for every node that you want to be queried.
