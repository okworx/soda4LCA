#!/bin/sh

if [[ -z $MYSQL_HOST ]]
then
    export MYSQL_HOST=mysqld
fi
if [[ -z $MYSQL_PORT ]]
then
    export MYSQL_PORT=3306
fi
if [[ -z $MYSQL_USER ]]
then
    export MYSQL_USER="soda-user"
fi
if [[ -z $MYSQL_PASSWORD ]]
then
    export MYSQL_PASSWORD="soda-pw"
fi
if [[ -z $MYSQL_DATABASE ]]
then
    export MYSQL_DATABASE="soda"
fi

until mysql --host=$MYSQL_HOST --user=$MYSQL_USER --password=$MYSQL_PASSWORD $MYSQL_DATABASE -e "select 1;"
do
    echo "Waiting for sql server ..."
    sleep 1
done

envsubst '${MYSQL_HOST},${MYSQL_PORT},${MYSQL_USER},${MYSQL_PASSWORD},${MYSQL_DATABASE},${MYSQL_DATABASE2}' < /opt/tomcat/conf/server.xml.template > /opt/tomcat/conf/server.xml

/opt/tomcat/bin/catalina.sh run
