package edu.kit.soda4lca.test.ui.admin;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.StringUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.codeborne.selenide.testng.ScreenShooter;

import static com.codeborne.selenide.Selenide.*;

import edu.kit.soda4lca.test.ui.AbstractUITest;
import edu.kit.soda4lca.test.ui.main.TestContext;
import edu.kit.soda4lca.test.ui.main.TestFunctions;

@Listeners({ScreenShooter.class})
public class T072PushStockTest extends AbstractUITest {
	
	// initializing the log
		protected final static Logger log = org.apache.log4j.Logger.getLogger(T072PushStockTest.class);
	
		@Override
		protected List<List<String>> getDBDataSetFileName() {
			return Arrays.asList(Arrays.asList( "DB_post_T071PushConfigTest.xml"), Arrays.asList("DB_pre_T015ImportExportTest.xml"));
		}
		
		@Override
		protected int getNumberOfConnections() {
			return 2;
		}
		
		/**
		 * {@inheritDoc}
		 * In this case (PushConfig is tested), the String "${nodeurl}" is replaced by the String targetSite set in TestContext. 
		 */
		@Override
		protected IDataSet preProcessDataSet(IDataSet dataSet) {
			log.debug("data set contains pushStockTest");
			ReplacementDataSet replacement = new ReplacementDataSet(dataSet);
			replacement.addReplacementObject("${nodeurl}", TestContext.SECONDARY_SITE_URL);
			return (IDataSet) replacement;
		}
		
		/**
		 * Logs in.
		 * @throws InterruptedException
		 */
		@BeforeClass
		public void login() throws InterruptedException {
			// import some data
			TestContext.getInstance().getDriver().manage().deleteAllCookies();
			log.debug("Trying to login");
			// login as admin
			TestFunctions.login("admin", "default", true, true);

			// click on Admin area
			TestFunctions.gotoAdminArea();
			// wait for the site to load
			TestFunctions.waitUntilSiteLoaded();
		}

		
	/**
	 * Tests push functionality of PushConfig and displayed job queue.
	 * @throws InterruptedException
	 */
	@Test(priority = 301)
	public void testDataPush() throws InterruptedException {
		dataStock_push("test_config_changed", false);
		dataStock_push("test_config_changed", true);
	}

	/**
	 * Tests display of imported data sets after last push in PushConfig.
	 * @throws InterruptedException
	 */
	@Test(priority = 302, dependsOnMethods = { "testDataPush" })
	public void testDataPushImport() throws InterruptedException {
		log.debug("Going to data import page.");
		TestFunctions.goToPageByAdminMenu("admin.dataImport", "admin.import");
		String path = this.getClass().getResource( "/sample_data_min.zip" ).getPath();
		String push_name = "test_config_changed";
		// if we are on a windows machine, the first / before the C:/ should be removed
		if ( File.separator.equals( "\\" ) )
			path = path.substring( 1 );
		path = path.replace( "/", java.io.File.separator );
		log.info( "loading file " + path );
		// wait for the site to load
		TestFunctions.waitUntilSiteLoaded();
		log.info( "found footer, sending keys " );
		TestContext.getInstance().getDriver().findElement( By.xpath( ".//*[@id='documentToUpload_input']" ) ).sendKeys( path );

		// click Upload
		// it's not a link but a button. So a little bit diMain.getInstance().getDriver()erent code
		log.info( "about to click 'upload'" );
		TestFunctions.findAndWaitOnElement( By.xpath( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.fileUpload.upload" ) + "')]" ) ).click();

		Thread.sleep(TestContext.wait * 20);
		
		// if uploaded successfully click 'Continue to step 2 (import)'
		TestFunctions.findAndWaitOnElement( By.xpath( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.importUpload.goToStep2" ) + "')]" ) ).click();
		// wait for the site to load
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By.linkText( TestContext.lang
				.getProperty( "public.user.logout" ) ) ) );
		// choose root data stock
//		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='tableForm:importRootDataStock_label']" ) ).click();
//		int i = 1;
//		while ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='tableForm:importRootDataStock_panel']/div/ul/li[" + i + "]" ) ).getText().equals(
//				"RootStock1" ) )
//			i++;
//		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='tableForm:importRootDataStock_panel']/div/ul/li[" + i + "]" ) ).click();
		
		// choose root data stock
	    $x(".//*[@id='tableForm:importRootDataStock_label']").click(); // open stock selection menu
	    Thread.sleep(TestContext.wait * 5);
		$x(".//*[@id='tableForm:importRootDataStock_panel']/div/ul/li[contains(.,'RootStock1')]").click(); // click on RootStock1
		Thread.sleep(TestContext.wait * 5);
		// next site, click on Import files
//		TestFunctions.findAndWaitOnElement( By.xpath( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.importFiles" ) + "')]" ) ).click();
		$x( "//button[contains(.,'" + TestContext.lang.getProperty( "admin.importFiles" ) + "')]" ).click();
		Thread.sleep(TestContext.wait * 5);
		log.debug( "Import started, it could take a while" );
		// then it starts importing. Status log is on an iframe, so change the driver
		switchTo().frame( "console" );

		TestFunctions.findAndWaitOnConsoleElement( By.xpath( "html/body[contains(.,'------ import of files finished ------')]" ) );
		
		switchTo().defaultContent();
		
		log.debug("Going to 'Manage PushConfig list' page.");
		
		TestFunctions.goToPageByAdminMenu("admin.network", "admin.push", "admin.pushConfig.manageList");
		
		Thread.sleep(TestContext.wait * 5);
		
		log.debug("Going to " + push_name + " entry.");
		TestFunctions.findAndWaitOnElement(By.linkText(push_name)).click();
		
		log.debug("Checking entry and selecting push putton.");
		
		if (!TestFunctions.elementWithIdEquals("showSourceDataSetCount", "12"))
			org.testng.Assert.fail("Incorrect count of  total data sets is shown. Expected: 12, actual: " + TestFunctions.findAndWaitOnElement(By.id("showSourceDataSetCount")).getText());
		
		if (!TestFunctions.elementWithIdEquals("showSourceImportedDataSets", "7"))
			org.testng.Assert.fail("Incorrect count of not pushed data sets is shown. Expected: 7, actual: " + TestFunctions.findAndWaitOnElement(By.id("showSourceDataSetCount")).getText());
		
		TestFunctions.clickButtonWithI18nLabel("admin.pushConfig.pushStocks");
		TestFunctions.fillInputText("pass", "default");
		TestFunctions.clickButtonWithI18nLabel("admin.ok");
		Thread.sleep(TestContext.wait * 5);
		
		//reloading page
		TestFunctions.goToPageByAdminMenu("admin.network", "admin.push", "admin.pushConfig.manageList");
		
		Thread.sleep(TestContext.wait * 5);
		
		log.debug("Going to " + push_name + " entry.");
		TestFunctions.findAndWaitOnElement(By.linkText(push_name)).click();
		
		
		if (!TestFunctions.elementWithIdEquals("showSourceImportedDataSets", "0"))
			org.testng.Assert.fail("Incorrect count of unpushed data sets is shown. Expected: 0, actual: " + TestFunctions.findAndWaitOnElement(By.id("showSourceDataSetCount")).getText());
	}
	
	/**
	 * Tries to push data stock. Checks after sending push call if all data stocks were transferred as expected and if no errors 
	 * occurred during push.
	 * @param action The action for selecting menu pages
	 * @param pushConfig The name of push config entry that shall be pushed
	 * @param alreadyPushed True if given push config entry was already pushed
	 * @throws InterruptedException
	 */
	public void dataStock_push(String pushConfig, boolean alreadyPushed) throws InterruptedException {
		String dependency = "admin.dependencies.ALL";
		String stockName = "RootStock1";
		log.debug("Going to 'Manage PushConfig list' page.");
		TestFunctions.goToPageByAdminMenu("admin.network", "admin.push", "admin.pushConfig.manageList");
		TestFunctions.waitUntilSiteLoaded();
		log.debug("Clicking push button.");
		TestFunctions.findAndWaitOnElement(By.linkText(pushConfig)).click();
		Thread.sleep(TestContext.wait * 5);
		TestFunctions.clickButtonWithI18nLabel("admin.pushConfig.pushStocks");
		TestFunctions.fillInputText("pass", "default");
		TestFunctions.clickButtonWithI18nLabel("admin.ok");
		Thread.sleep(TestContext.wait * 5);

		log.debug("Checking entry information after push. Since race condictions can occur, page is needed to be callled again.");
		if (!TestFunctions.elementWithIdEquals("pushNameOut", pushConfig))
			org.testng.Assert.fail("Incorrect push config name is shown.");
		
		if (!TestFunctions.elementWithIdEquals("dependenciesModeOut", TestContext.lang.getProperty(dependency)))
			org.testng.Assert.fail("Incorrect dependency mode is shown.");
		
		if (!TestFunctions.elementWithIdEquals("showSourceID", stockName))
			org.testng.Assert.fail("Incorrect target data stock is shown.");
		
		if (!TestFunctions.elementWithIdEquals("showSourceDataSetCount", "5"))
			org.testng.Assert.fail("Incorrect count of data sets is shown.");

			TestFunctions.goToPageByAdminMenu("admin.network", "admin.push", "admin.pushConfig.manageList");
			TestFunctions.findAndWaitOnElement(By.linkText(pushConfig)).click();
			TestFunctions.waitUntilSiteLoaded();
			

		if (!TestFunctions.elementWithIdEquals("showSourceImportedDataSets", "0"))
			org.testng.Assert.fail("Incorrect count of data sets is shown.");
		TestFunctions.goToPageByAdminMenu("admin.jobs", "admin.jobs.showList");
		log.debug("Going to 'Show jobs' page.");
		String state = TestFunctions.getFirstTableEntryAtColumn("showJobsTable", new Long(11));
		
		log.debug("Checking new job entry.");
		Thread.sleep(TestContext.wait * 5);
		if (!TestFunctions.isLastTableEntryAtColumnEqual("showJobsTable", new Long(2), "admin")) {
			org.testng.Assert.fail("Incorrect user name is shown.");
		}
		if (!TestFunctions.isLastTableEntryAtColumnEqual("showJobsTable", new Long(3), "push")) {
			org.testng.Assert.fail("Incorrect job type is shown.");
		}
		String description = stockName + " to default in ACME2" ;
		
		if (!TestFunctions.isLastTableEntryAtColumnEqual("showJobsTable", new Long(4), description)) {
			org.testng.Assert.fail("Incorrect description is shown.");
		}
		if (log.isDebugEnabled())
			log.debug("state is: '" + state + "'");
		while (state.equals("RUNNING")) {
			TestFunctions.clickButtonWithLabel("refresh");
			TestFunctions.waitUntilSiteLoaded();
			state = TestFunctions.getFirstTableEntryAtColumn("showJobsTable", new Long(11));
			log.debug("updating job page");
		}
		log.debug("job is complete.");
		Long errorCount = Long.valueOf( TestFunctions.getFirstTableEntryAtColumn("showJobsTable", new Long(8))).longValue();
		Long infoCount = Long.valueOf(TestFunctions.getFirstTableEntryAtColumn("showJobsTable", new Long(9))).longValue();
		Long successCount = Long.valueOf(TestFunctions.getFirstTableEntryAtColumn("showJobsTable", new Long(10))).longValue();
		

		TestFunctions.findAndWaitOnElement(By.xpath(".//*[@id='showJobsTable_data']/tr[1]/td[1]/div")).click();
		String jobLog = TestFunctions.getSecondTableEntryAtColumn("showJobsTable", new Long(1));
		if (!alreadyPushed) {
			log.debug("Checking amount of data sets that were pushed correctly.");
			if (errorCount != 0 || infoCount != 0 || successCount != 5 || !(state.equals("COMPLETE"))) {
				if (log.isTraceEnabled()) {
					log.trace("Error is: " + errorCount);
					log.trace("infosCount is: " + infoCount);
					log.trace("state is: " + state);
				}
				org.testng.Assert.fail("Job did not complete correctly!");
			}
			log.debug("Checking job log.");
			
			Long pushedDataSets = new Long(StringUtils.countOccurrencesOf(jobLog, "SUCCESS"));
			if (log.isTraceEnabled()) {
				log.trace("jobLog is: " + jobLog);
				log.trace("number of pushed data sets is: " + pushedDataSets);
			}
			if (pushedDataSets != 6) {
				org.testng.Assert.fail("Incorrect ammount of files was correctly pushed!");
			}
		} else {
			log.debug("Checking amount of data sets that were already pushed.");
			if (errorCount != 0 || infoCount != 5 || successCount != 0|| !(state.equals("COMPLETE"))) {
				org.testng.Assert.fail("Job did not complete correctly!");
			}
			
			int pushedDataSets = StringUtils.countOccurrencesOf(jobLog, "INFO");
			if (pushedDataSets != 6) {
				if (log.isDebugEnabled()) {
					log.debug("number of failed pushes: " + StringUtils.countOccurrencesOf(jobLog, "ERROR"));
					log.debug("number of successful pushes: " + StringUtils.countOccurrencesOf(jobLog, "SUCCESS"));
					log.debug("Number of already pushed stocks: " + StringUtils.countOccurrencesOf(jobLog, "INFO"));
				}
				org.testng.Assert.fail("A data set was unexpectedly not pushed!");
			}
		}
		
		log.debug("Checking on target node whether all pushed datasets appear in correct data stock.");
		TestFunctions.login("admin", "default", true, true, 0, false, TestContext.SECONDARY_SITE_URL, null);
		
		TestFunctions.gotoAdminArea();
		TestFunctions.waitUntilSiteLoaded();
		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.process.manageList");
		
		TestFunctions.targetTableContainsNumberDataSets("processTable", new Long(5));
		
		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.lciaMethod.manageList");
		TestFunctions.targetTableContainsNumberDataSets("lciamethodTable", new Long(0));
		
		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.elementaryFlow.manageList");
		TestFunctions.targetTableContainsNumberDataSets("flowTable", new Long(0));
		
		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.productFlow.manageList");
		TestFunctions.targetTableContainsNumberDataSets("flowTable", new Long(0));
		
		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.flowProperty.manageList");
		TestFunctions.targetTableContainsNumberDataSets("flowpropertyTable", new Long(0));

		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.unitGroup.manageList");
		TestFunctions.targetTableContainsNumberDataSets("unitgroupTable", new Long(0));

		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.source.manageList");
		TestFunctions.targetTableContainsNumberDataSets("sourceTable", new Long(0));

		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.contact.manageList");
		TestFunctions.targetTableContainsNumberDataSets("contactTable", new Long(1));

		login();
	}

}
