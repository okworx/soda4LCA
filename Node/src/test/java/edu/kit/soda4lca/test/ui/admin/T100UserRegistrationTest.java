package edu.kit.soda4lca.test.ui.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.codeborne.selenide.testng.ScreenShooter;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;

import edu.kit.soda4lca.test.ui.AbstractUITest;
import edu.kit.soda4lca.test.ui.main.TestContext;
import edu.kit.soda4lca.test.ui.main.TestFunctions;

@Listeners({ScreenShooter.class})
public class T100UserRegistrationTest extends AbstractUITest {

	// initializing the log
	protected final static Logger log = org.apache.log4j.Logger.getLogger(T100UserRegistrationTest.class);

	@Override
	protected List<List<String>> getDBDataSetFileName() {
		return Arrays.asList(Arrays.asList("DB_pre_T012UsersTest.xml"));
	};

	private GreenMail mailServer;

	private String siteName = "@APP-TITLE@";

	WebDriver driver;

	int mailCount = 0;

	@AfterClass
	public void tearDownMailServer() {
		mailServer.stop();
	}

	/**
	 * Sets up target node
	 * 
	 * @param nodeExisting
	 *            true if target node already exists
	 * @param action
	 *            The action for retrieving page in admin menu
	 * @throws Exception
	 */
	@BeforeClass
	public void setup() throws Exception {
		super.setup();
		ServerSetup setup = new ServerSetup(3025, "localhost", "smtp");
		mailServer = new GreenMail(setup);
		mailServer.start();
		log.info("Begin of UserRegistration test");
		TestContext.getInstance().getDriver().manage().deleteAllCookies();
		driver = TestContext.getInstance().getDriver();
		// open the main site
		driver.manage().deleteAllCookies();
		driver.get(TestContext.PRIMARY_SITE_URL);
		Thread.sleep(TestContext.wait);
	}

	/**
	 * Tests creation of PushTarget with several (valid and invalid) settings.
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws MessagingException
	 */
	@Test(priority = 101)
	public void testRequired() throws InterruptedException, MessagingException, IOException {
		log.info("Begin to test required fields.");
		checkRequiredFields("user", "user@user.user", false, true, true);
		checkRequiredFields(null, "user@user.user", false, true, true);
		checkRequiredFields("user2", null, false, true, true);
		checkRequiredFields("user3", "qwerty", false, false, true);
		checkRequiredFields("user", "user@user.user", true, true, true);
		checkRequiredFields("user5", "user@user.user", false, true, false);
		log.info("Finished testing required fields.");
	}

	/**
	 * Tests editing of PushTarget entry with several (valid and invalid)
	 * settings.
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws MessagingException
	 */
	@Test(priority = 102)
	public void testAllFields() throws InterruptedException, MessagingException, IOException {
		String name = "test_user";
		log.info("Begin to test all registration fields");
		log.debug("Going to 'Registration' page.");
		if (TestFunctions.isElementNotPresent(By.xpath(".//*[@id='leftFooter']/a[3]"))) {
			TestFunctions.logout();
		}
		TestFunctions.findAndWaitOnElement(By.linkText(TestContext.lang.getProperty("public.user.register"))).click();
		log.debug("Filling in form.");
		TestFunctions.fillInputText("nameIn", name);
		TestFunctions.fillInputText("email", "user@user.user");

		TestFunctions.fillInputText("title", "PhD");
		TestFunctions.fillInputText("firstName", "John");
		TestFunctions.fillInputText("lastName", "Doe");
		TestFunctions.findAndWaitOnElement(By.xpath(".//table[@id='gender']/tbody/tr[1]/td[2]/div")).click();
		TestFunctions.fillInputText("jobposition", "chief");
		TestFunctions.fillInputText("institution", "institute of sciences");
		TestFunctions.fillInputText("phone", "+335557962375");
		TestFunctions.findAndWaitOnElement(By.xpath(".//textarea[@id='dspurpose']")).clear();
		TestFunctions.findAndWaitOnElement(By.xpath(".//textarea[@id='dspurpose']")).sendKeys("for data mining");

		TestFunctions.selectItemInSelectBox("country", "Faroe Islands");
		TestFunctions.fillInputText("zipCode", "96532");
		TestFunctions.fillInputText("city", "London");
		TestFunctions.findAndWaitOnElement(By.xpath(".//textarea[@id='streetAddr']")).clear();
		TestFunctions.findAndWaitOnElement(By.xpath(".//textarea[@id='streetAddr']")).sendKeys("Baker Street 221b");

		int addTerms = TestContext.getInstance().getDriver()
				.findElements(By.xpath(".//div[@id='additionalTerms_content']/table/tbody/tr")).size();

		for (int i = 1; i <= addTerms; i++) {
			TestFunctions.findAndWaitOnElement(By.xpath(".//div[@id='additionalTerms_content']/table/tbody/tr[" + i
					+ "]/td/div/div[2]/table/tbody/tr/td[1]/div")).click();
		}

		if (TestFunctions
				.isElementNotPresent(By.xpath(".//*[@id='privacyPolicy']/tbody/tr/td[3]/label/span[contains(.,'*')]"))) {
			
			TestFunctions.findAndWaitOnElement(By.id("acceptPrivacyPolicy")).click();
		}
		TestFunctions.clickButtonWithI18nLabel("public.user.register");
		Thread.sleep(TestContext.wait);

		if (!(TestFunctions.findAndWaitOnElement(By.xpath(".//h1")).getText()
				.equals(TestContext.lang.getProperty("public.user.registrated.msg")))) {
			org.testng.Assert.fail("Web page for showing successfull registration is not shown.");
		}

		WebDriver driver = TestContext.getInstance().getDriver();

		checkMailLogin(name, driver, ++mailCount);

		TestFunctions.logout();

		List<Integer> addTermsList = new ArrayList<Integer>();
		for (int i = 1; i <= addTerms; i++) {
			addTermsList.add(i);
		}
		checkExistsAndAddTerms(name, true, addTermsList);

		log.info("End of testing all fields");

	}

	/**
	 * Tests deletion of several PusTarget entries.
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws MessagingException
	 */
	@Test(priority = 103)
	public void testAddTerms() throws InterruptedException, MessagingException, IOException {
		log.info("Begin of testing additional terms.");
		checkAddTerms("firstUser", "email@email.com", Arrays.asList(1));
		checkAddTerms("secondUser", "email@email.com", Arrays.asList(2));
		checkAddTerms("thirdUser", "email@email.com", Arrays.asList(1, 2));
		checkAddTerms("fourthUser", "email@email.com", Arrays.asList());
		log.info("Finished testing additional terms.");
	}

	/**
	 * Tries to create a pushTarget entry and test whether pushTarget is created
	 * correctly or pushTarget is not created if at least one input was
	 * incorrect
	 * 
	 * @param name
	 *            The name of push target
	 * @param node
	 *            The name of target node
	 * @param login
	 *            The user name of target user
	 * @param passw
	 *            The password of target user
	 * @param selectTargetStock
	 *            True if target stock is selected
	 * @param nodeLaterChanged
	 *            True if target node is set to null after available target
	 *            stcoks are shown
	 * @param loginPasswCorrect
	 *            True if the combination of user name and password is correct
	 * @param nodeExisting
	 *            True if target node with given name already exists
	 * @param targetExisting
	 *            True if pushTarget entry with given name already exists
	 * @throws InterruptedException
	 * @throws MessagingException
	 * @throws IOException
	 */
	public void checkRequiredFields(String name, String email, boolean userAlreadyExisting, boolean emailCorrect,
			boolean privacyPolicyAccepted) throws InterruptedException, MessagingException, IOException {
		if (TestFunctions.isElementNotPresent(By.xpath(".//*[@id='leftFooter']/a[3]"))) {
			TestFunctions.logout();
		}
		log.debug("Going to 'Registration' page.");
		TestFunctions.findAndWaitOnElement(By.linkText(TestContext.lang.getProperty("public.user.register"))).click();
		log.debug("Filling in form.");
		TestFunctions.fillInputText("nameIn", name);
		TestFunctions.fillInputText("email", email);

		if (privacyPolicyAccepted) {
			if (TestFunctions
					.isElementNotPresent(By.xpath(".//*[@id='privacyPolicy']/tbody/tr/td[3]/label/span[contains(.,'*')]"))) {
				TestFunctions.findAndWaitOnElement(By.id("acceptPrivacyPolicy")).click();
			}
		}

		boolean privacyPolicyRequired = TestFunctions
				.isElementNotPresent(By.xpath(".//*[@id='privacyPolicy']/tbody/tr/td[3]/label/span[contains(.,'*')]"));
		
		if (log.isDebugEnabled()) {
			log.debug("privacy policy required: " + privacyPolicyRequired);
		}

		log.debug("Trying to register new user.");
		TestFunctions.clickButtonWithI18nLabel("public.user.register");
		Thread.sleep(TestContext.wait);

		if (name == null || name.isEmpty() || email == null || email.isEmpty() || !emailCorrect || userAlreadyExisting
				|| (!privacyPolicyAccepted && privacyPolicyRequired)) {
			if ((name == null || name.isEmpty()) && !TestFunctions.isMessageShown("admin.user.requiredMsg.loginName")) {
				org.testng.Assert.fail("name is empty but not corresponding error message is shown.");
			}
			if ((email == null || email.isEmpty()) && !TestFunctions.isMessageShown("admin.user.requiredMsg.email")) {
				org.testng.Assert.fail("e-mail address is empty but no corresponding error message is shown.");
			}
			if (!emailCorrect && !TestFunctions.isMessageShown("admin.user.validatorMsg.email")) {
				org.testng.Assert.fail("email address has not correct format but no erroro message is shown.");
			}

			if (userAlreadyExisting && !TestFunctions.isMessageShown("facesMsg.user.alreadyExists"))
				if (!privacyPolicyAccepted && !TestFunctions.isMessageShown("admin.user.acceptPrivacyPolicyRequired")) {
					org.testng.Assert
							.fail("Privacy policy was not accepted but no corresponding error message is shown.");
				}

		} else {

			if (!(TestFunctions.findAndWaitOnElement(By.xpath(".//h1")).getText()
					.equals(TestContext.lang.getProperty("public.user.registrated.msg"))))
				org.testng.Assert.fail("Web page for showing successfull registration is not shown.");

			WebDriver driver = TestContext.getInstance().getDriver();

			checkMailLogin(name, driver, ++mailCount);

			TestFunctions.logout();

		}

		TestFunctions.login("admin", "default", true, true);

		TestFunctions.gotoAdminArea();

		TestFunctions.goToPageByAdminMenu("admin.user", "admin.user.manageList");

		if (name != null && !name.isEmpty()) {
			boolean exists = !(email == null || email.isEmpty() || !emailCorrect || (!privacyPolicyAccepted && privacyPolicyRequired));
			checkExistsAndAddTerms(name, exists, null);
		}

		TestFunctions.logout();
		log.info("End of fill in required fields.");
	}

	/**
	 * Tries to change target. Tests whether pushTarget is changed correctly or
	 * not changed if at least one input was incorrectly set.
	 * 
	 * @param name
	 *            The new name of pushTarget entry
	 * @param login
	 *            The user name of target node
	 * @param passw
	 *            The password of target user
	 * @param stockSelected
	 *            True if source stock is selected
	 * @param loginPasswCorrect
	 *            True if the combination of user name and password is correct
	 * @param nameIllegalChanged
	 *            True if new name is not accepted as pushTarget name
	 * @param nodeChanged
	 *            True if node is changed to null after target stock is selected
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws MessagingException
	 */
	public void checkAddTerms(String name, String email, List<Integer> addTerms)
			throws InterruptedException, MessagingException, IOException {
		if (TestFunctions.isElementNotPresent(By.xpath(".//*[@id='leftFooter']/a[3]"))) {
			TestFunctions.logout();
		}
		log.debug("Going to 'Registration' page.");
		TestFunctions.findAndWaitOnElement(By.linkText(TestContext.lang.getProperty("public.user.register"))).click();
		log.debug("Filling in form.");
		TestFunctions.fillInputText("nameIn", name);
		TestFunctions.fillInputText("email", email);

		for (int i = 0; i < addTerms.size(); i++) {
			TestFunctions.findAndWaitOnElement(By.xpath(".//div[@id='additionalTerms_content']/table/tbody/tr["
					+ addTerms.get(i) + "]/td/div/div[2]/table/tbody/tr/td[1]/div")).click();
		}

		if (TestFunctions
				.isElementNotPresent(By.xpath(".//*[@id='privacyPolicy']/tbody/tr/td[3]/label/span[contains(.,'*')]"))) {
			TestFunctions.findAndWaitOnElement(By.id("acceptPrivacyPolicy")).click();
		}

		log.debug("Trying to login to target page for showing available target stocks.");
		TestFunctions.clickButtonWithI18nLabel("public.user.register");
		Thread.sleep(TestContext.wait);

		WebDriver driver = TestContext.getInstance().getDriver();

		checkMailLogin(name, driver, ++mailCount);

		TestFunctions.logout();

		checkExistsAndAddTerms(name, true, addTerms);

		log.info("End of checking additional terms.");

	}

	/**
	 * Checks registration mail which was sent to registered user and checks
	 * whether user can login with given credentials-
	 * 
	 * @param name
	 *            The name of registered user
	 * @param driver
	 *            The WebDriver managing to navigate to given web page
	 * @param count
	 *            The total count of messages in email box
	 * @throws MessagingException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private void checkMailLogin(String name, WebDriver driver, int count)
			throws MessagingException, IOException, InterruptedException {
		MimeMessage[] messages = mailServer.getReceivedMessages();
		log.debug("Beginning to check sent mail.");

		if (messages.length != count) {
			org.testng.Assert.fail("E-mail server should get only " + count + " mail, but didn't (Got "
					+ messages.length + " mails).");
		}

		if (log.isDebugEnabled()) {
			log.debug("count " + count);
		}

		if (!messages[count - 1].getSubject().equals("User registration at " + siteName)) {
			org.testng.Assert.fail("Subject should be 'User registration at " + siteName + "', but is '"
					+ messages[0].getSubject() + "'.");
		}

		if (log.isDebugEnabled()) {
			log.debug("user name in mail: '" + TestFunctions.extractUserFomMail(messages[count - 1])
					+ "', actual user name: '" + name + "'");
		}
		if (!TestFunctions.extractUserFomMail(messages[count - 1]).equals(name)) {
			org.testng.Assert.fail("In e-mail is wrong login name shown.");
		}

		String passw = TestFunctions.extractPasswordFomMail(messages[count - 1]);
		String link = TestFunctions.extractLinkFomMail(messages[count - 1]);
		String newPassw = "S3cr3tPassword";

		driver.get(link);

		if (!TestFunctions.isMessageShown("facesMsg.activation.accountActivatedLoggedIn")) {
			org.testng.Assert.fail(
					"The registrated account could not be activated! Error message is: " + TestFunctions.getMessage());
		}

		TestFunctions.logout();

		log.debug("Trying to login with credentials given from registration mail.");
		TestFunctions.login(name, passw, true, true);

		if (!TestFunctions.isElementNotPresent(By.xpath(".//*[@id='leftFooter']/a[3]"))) {
			org.testng.Assert.fail("user was not correctly logged in!");
		}
		
		TestFunctions.logout();
		
		log.debug("Trying to login with expired password.");
		TestFunctions.loginWithExpiredPassword(name, passw, newPassw, true, true);
		
		if (!TestFunctions.isElementNotPresent(By.xpath(".//*[@id='leftFooter']/a[3]"))) {
			org.testng.Assert.fail("user was not correctly logged in with expired password!");
		}
		
	}

	/**
	 * Checks whether user with given name exists and whether corresponding
	 * additional terms are checked.
	 * 
	 * @param name
	 *            The name of registered user.
	 * @param exists
	 *            The flag indicating whether user was registered or not
	 * @param addTerms
	 *            The list of numbers which indicate which additional term was
	 *            checked
	 * @throws InterruptedException
	 */
	private void checkExistsAndAddTerms(String name, boolean exists, List<Integer> addTerms)
			throws InterruptedException {
		TestFunctions.login("admin", "default", true, true);

		TestFunctions.gotoAdminArea();

		TestFunctions.goToPageByAdminMenu("admin.user", "admin.user.manageList");
		log.debug(
				"Checkiung whether user with given name exists in user list and whether correct additional terms are checked.");
		if (exists) {
			if (!TestFunctions.isElementNotPresent(By.linkText(name))) {
				org.testng.Assert.fail("User " + name + " should be registred but seems to be not.");
			}

			int addTermsBegin = 11;
			if (addTerms != null) {

				for (int i = 0; i < addTerms.size(); i++) {
					if (!TestFunctions.isElementNotPresent(By.xpath(".//*[text()='" + name + "']/../../td["
							+ (addTermsBegin + addTerms.get(i)) + "]/div/span[@class='ui-icon ui-icon-check']"))) {
						org.testng.Assert.fail("AdditionalTerm no. " + addTerms.get(i) + " for user " + name
								+ " should be checked but is not!");
					}
				}
			}

		} else {
			if (TestFunctions.isElementNotPresent(By.linkText(name))) {
				org.testng.Assert.fail("User " + name + " should not be registred but seems to be.");
			}
		}

	}

}
