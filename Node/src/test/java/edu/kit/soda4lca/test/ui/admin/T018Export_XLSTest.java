package edu.kit.soda4lca.test.ui.admin;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.codeborne.selenide.testng.ScreenShooter;

import edu.kit.soda4lca.test.ui.AbstractUITest;
import edu.kit.soda4lca.test.ui.main.FileTestFunctions;
import edu.kit.soda4lca.test.ui.main.TestContext;
import edu.kit.soda4lca.test.ui.main.TestFunctions;
import edu.kit.soda4lca.test.ui.main.WorkbookTestFunctions;

/**
 * Checks the export of a data set by counting the number of entries in download excel file 
 * and comparing it with the number of data set entries
 * @author Sarai Eilebrecht
 *
 */
@Listeners({ScreenShooter.class})
public class T018Export_XLSTest extends AbstractUITest {
	
	// initializing the log
	protected final static Logger log = org.apache.log4j.Logger.getLogger( T018Export_XLSTest.class );

	@Override
	protected List<List<String>> getDBDataSetFileName() { return Arrays.asList(Arrays.asList("DB_post_T015ImportExportTest.xml")); };

	@BeforeClass
	public void login() throws InterruptedException, JsonGenerationException, JsonMappingException, IOException {	
		TestContext.getInstance().getDriver().manage().deleteAllCookies();
		log.debug("all Cookies deleted.");
		// login as admin
		TestFunctions.login( "admin", "default", true, true );
		// click on Admin area
		TestFunctions.gotoAdminArea();
		
		// wait for the site to load
		log.debug("Went to admin area");
		TestFunctions.waitUntilSiteLoaded();
		//select RootStock 1
		TestFunctions.selectDataStock("RootStock1");
	}
	
	/**
	 * Checks number of exported xls file of Process data sets that are visible on current page
	 * @throws InterruptedException
	 * @throws IOException
	 */
	 @Test
	 public void exportProcessesPageDataAsXLS() throws InterruptedException, IOException{
		 exportDataAsXLS("processes", "pageOnly");
	 }
	 
	 /**
	  * Checks number of exported xls file of all Process data sets
	  * @throws InterruptedException
	  * @throws IOException
	  */
	 @Test
	 public void exportProcessesAllDataAsXLS() throws InterruptedException, IOException{
		 exportDataAsXLS("processes", "all");
	 }
	 
	 /**
	  * Checks number of exported xls file of LCIA Methods data sets that are visible on current page
	  * @throws InterruptedException
	  * @throws IOException
	  */
	 @Test
	 public void exportLCIAMethodsPageDataAsXLS() throws InterruptedException, IOException{
		 exportDataAsXLS("lciaMethods", "pageOnly");
	 }
	 
	 /**
	  * Checks number of exported xls file of all LCIA Methods data sets
	  * @throws InterruptedException
	  * @throws IOException
	  */
	 @Test
	 public void exportLCIAMethodsAllDataAsXLS() throws InterruptedException, IOException{
		 exportDataAsXLS("lciaMethods", "all");
	 }
	 
	 /**
	  * Checks number of exported xls file of Elementary Flows data sets that are visible on current page
	  * @throws InterruptedException
	  * @throws IOException
	  */
	 @Test
	 public void exportElementaryFlowsPageDataAsXLS() throws InterruptedException, IOException{
		 exportDataAsXLS("elementaryFlows", "pageOnly");
	 }
	 
	 
	/**
	 * Checks number of exported xls file of all Elementary Flows data sets
	 * @throws InterruptedException
	 * @throws IOException
	 */
	 @Test
	 public void exportElementaryFlowsAllDataAsXLS() throws InterruptedException, IOException{
		 exportDataAsXLS("elementaryFlows", "all");
	 }
	 
	 /**
	  * Checks number of exported xls file of Poduct Flows data sets that are visible on current page
	  * @throws InterruptedException
	  * @throws IOException
	  */
	 @Test
	 public void exportProductFlowsPageDataAsXLS() throws InterruptedException, IOException{
		 exportDataAsXLS("productFlows", "pageOnly");
	 }
	 
	 /**
	  * Checks number of exported xls file of all Product Flows data sets
	  * @throws InterruptedException
	  * @throws IOException
	  */
	 @Test
	 public void exportProductFlowsAllDataAsXLS() throws InterruptedException, IOException{
		 exportDataAsXLS("productFlows", "all");
	 }
	 
	 /**
	  * Checks number of exported xls file of Flow Properties data sets that are visible on current page
	  * @throws InterruptedException
	  * @throws IOException
	  */
	 @Test
	 public void exportFlowPropertiesPageDataAsXLS() throws InterruptedException, IOException{
		 exportDataAsXLS("flowProperties", "pageOnly");
	 }
	 
	 /**
	  * Checks number of exported xls file of all Flow Properties data sets
	  * @throws InterruptedException
	  * @throws IOException
	  */
	 @Test
	 public void exportFlowPropertiesAllDataAsXLS() throws InterruptedException, IOException{
		 exportDataAsXLS("flowProperties", "all");
	 }
	 
	 /**
	  * Checks number of exported xls file of Unit Groups data sets that are visible on current page
	  * @throws InterruptedException
	  * @throws IOException
	  */
	 @Test
	 public void exportUnitGroupsPageDataAsXLS() throws InterruptedException, IOException{
		 exportDataAsXLS("unitGroups", "pageOnly");
	 }
	 
	 /**
	  * Checks number of exported xls file of all Unit Groups data sets
	  * @throws InterruptedException
	  * @throws IOException
	  */
	 @Test
	 public void exportUnitGroupsAllDataAsXLS() throws InterruptedException, IOException{
		 exportDataAsXLS("unitGroups", "all");
	 }
	 
	 /**
	  * Checks number of exported xls file of Sources data sets that are visible on current page
	  * @throws InterruptedException
	  * @throws IOException
	  */
	 @Test
	 public void exportSourcesPageDataAsXLS() throws InterruptedException, IOException{
		 exportDataAsXLS("sources", "pageOnly");
	 }
	 
	 /**
	  * Checks number of exported xls file of all Sources data sets
	  * @throws InterruptedException
	  * @throws IOException
	  */
	 @Test
	 public void exportSourcesAllDataAsXLS() throws InterruptedException, IOException{
		 exportDataAsXLS("sources", "all");
	 }
	 
	 /**
	  * Checks number of exported xls file of Contacts data sets that are visible on current page
	  * @throws InterruptedException
	  * @throws IOException
	  */
	 @Test
	 public void exportContactsPageDataAsXLS() throws InterruptedException, IOException{
		 exportDataAsXLS("contacts", "pageOnly");
	 }
	 
	 /**
	  * Checks number of exported xls file of all Contacts data sets
	  * @throws InterruptedException
	  * @throws IOException
	  */
	 @Test
	 public void exportContactsAllDataAsXLS() throws InterruptedException, IOException{
		 exportDataAsXLS("contacts", "all");
	 }
	 /**
		 * Checks the number of entries in download excel file against count of data sets on current page or all data sets
		 * 
		 * @throws InterruptedException
		 * @throws IOException 
		 */
		 
		public void exportDataAsXLS(String data, String exportType) throws InterruptedException, IOException {
			log.info( "Export " + data + exportType +" data set to xls test started" );
			// export data as xls file
			String stockName = TestContext.lang.getProperty("common." + data).replaceAll(" ", "_");
			String fileName = ("Node_RootStock1_" + stockName + "_" + TestContext.lang.getProperty("admin.export.datasets." + exportType + ".suffix") + ".xls").replaceAll("\\+", "_");
			FileTestFunctions.tryDeleteFile(fileName, this.getClass());
	
			TestFunctions.selectDataSet(data);
			
			//detect number of data sets on current page
			int dataSetEntries = TestFunctions.countTotalDatasetEntries();
			
			//if only entries of current page shall be exported and number of entries is bigger than the number of visible entries, 
			//take number of visible entries as reference
			if (exportType == "pageOnly") {
				int pageCount = TestFunctions.countDatasetEntriesOnPage();
				if (pageCount < dataSetEntries) {
					dataSetEntries = pageCount;
				}
			}
			
			//click on Export As SLX file button
			TestFunctions.findAndWaitOnElement(By.xpath(".//div[@class='floatRight']/span[1]/button[1]")).click();
			//select given export option in drop-down menu
			TestFunctions.findAndWaitOnElement(By.linkText(TestContext.lang.getProperty("admin.export.datasets." + exportType))).click();
			Thread.sleep(TestContext.wait * 20);
			//Count number of entries in download excel file and compare result with number of data set entries
			WorkbookTestFunctions.countAndCompareEntries(fileName, this.getClass(), dataSetEntries);
			
			log.info( "Export " + data + exportType + " data set to xls test finished" );
		}

}
