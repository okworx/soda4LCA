package edu.kit.soda4lca.test.ui.admin;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.codeborne.selenide.testng.ScreenShooter;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;

import edu.kit.soda4lca.test.ui.AbstractUITest;
import edu.kit.soda4lca.test.ui.main.TestContext;
import edu.kit.soda4lca.test.ui.main.TestFunctions;

/**
 * Tests the assign functionality for API.
 * @author sarai
 *
 */
@Listeners({ScreenShooter.class})
public class T120DatasetAssignAPITest extends AbstractUITest {
	
	// initializing the log
	protected final static Logger log = org.apache.log4j.Logger.getLogger(T120DatasetAssignAPITest.class);

	@Override
	protected List<List<String>> getDBDataSetFileName() {
		return Arrays.asList(Arrays.asList("DB_pre_T120DatasetAssignAPITest_fragment.xml", "DB_post_T015ImportExportTest.xml"));
	};
	
	private static final String WRITING_USER = "Admin1";
	private static final String READING_USER = "User3";
	
	private static final String STOCK_ID = "8e771302-b68e-4ce3-ba04-cb0e2095dc0e";
	private static final String ROOT_STOCK_ID = "9e2c6d39-daee-49d0-9d38-783100116aac";
	
	private static final String WRONG_VERION_FORMAT = "343eqe.6gfhfg.90sfs";
	
	private static final String PASSWORD = "s3cr3t";

	private static final String HEADER_NAME = "Authorization";
	
	private int correct_dependency = 3;
	private int wrong_dependency = 5;
	
	/*
	 * The network client to connect to the REST API.
	 */
	Client client;
	
	WebDriver driver;
	
	/**
	 * Sets up the test by logging in and creating a network client.
	 */
	@BeforeClass
	public void setup() throws Exception {
		super.setup();
		log.info("Begin of DatasetAssignAPI test");

		client = TestFunctions.createClient();
	}
	
	@Test(priority=101)
	public void testProcesses() throws InterruptedException {
		log.info("begin to test assigning processes");
		String type = "processes";
		String assignedDataset = "1a7da06d-e8b7-4ff1-920c-209e9009dbe0";
		
		assignAnonymous(type, assignedDataset);
		assignNotWrite(type, assignedDataset);
		wrongDataStockID(type, assignedDataset);
		wrongDatasetID(type, "5296e2be-060b-4e50-b033-d45f85f6ac92");
		rootDataStock(type, assignedDataset);
		wrongVersion(type, assignedDataset, "02.00.000");
		wrongVersionFormat(type, assignedDataset);
		wrongDependency(type, assignedDataset);
		
		assign(type, assignedDataset);
		assignVersion(type, "4cffadc4-e2a2-4d34-9087-e54a2cfa4bd0", "03.00.000");
		
		String[] dependencies = {"16e9b2c6-3c44-11dd-ae16-0800200c9a66"};
		String[] dependencyType = {"contacts"};
		assignDependencies(type, "3c0a1214-f4b2-4254-8fca-1d9e6ee9839f", dependencies, dependencyType);
		
		alreadyExisting(type, assignedDataset);
		log.info("end of test assigning processes");
		
	}
	
	@Test(priority=102)
	public void testLICIAMethods() throws InterruptedException {
		log.info("begin to test assigning LCIA methods");
		String type= "lciamethods";
		String assignedDataset = "edabaa8b-89d0-4cb6-ba92-444ac5265422";

		assignAnonymous(type, assignedDataset);
		assignNotWrite(type, assignedDataset);
		wrongDataStockID(type, assignedDataset);
		wrongDatasetID(type, "3c0a1214-f4b2-4254-8fca-1d9e6ee9839f");
		rootDataStock(type, assignedDataset);
		wrongVersion(type, assignedDataset, "02.00.000");
		wrongVersionFormat(type, assignedDataset);
		wrongDependency(type, assignedDataset);
		
		assign(type, assignedDataset);
		assignVersion(type, "b7d61a6f-cb2f-4a46-b511-39c3e4cc31d3", "01.00.000");
		
//		Dependencies is currently not supported
//		String[] dependencies = {"16e9b2c6-3c44-11dd-ae16-0800200c9a66"};
//		String[] dependencyType = {"contacts"};
//		assignDependencies(type, "992c8e8d-769a-4930-9b0f-4fa323250738", dependencies, dependencyType);
		alreadyExisting(type, assignedDataset);
		log.info("end of test assigning LCIA methods");
	}
	
	@Test(priority=103)
	public void testElementaryFlows() throws InterruptedException {
		log.info("begin to test assigning elementary flows");
		String type = "flows";
		String assignedDataset = "0a2e688c-b9bd-416c-a25f-9446815aefa3";

		assignAnonymous(type, assignedDataset);
		assignNotWrite(type, assignedDataset);
		wrongDataStockID(type, assignedDataset);
		wrongDatasetID(type, "3c0a1214-f4b2-4254-8fca-1d9e6ee9839f");
		rootDataStock(type, assignedDataset);
		wrongVersion(type, assignedDataset, "02.00.000");
		wrongVersionFormat(type, assignedDataset);
		wrongDependency(type, assignedDataset);
		
		assign(type, assignedDataset);
		assignVersion(type, "0a2a71f2-4f59-4a67-b5f1-d633039ce0ea", "03.00.000");
		
//		Dependencies is currently not supported
//		String[] dependencies = {"16e9b2c6-3c44-11dd-ae16-0800200c9a66"};
//		String[] dependencyType = {"contacts"};
//		assignDependencies(type, "0a0c1d5d-a396-49da-8ee0-9d4c2a06ea1f");
		alreadyExisting(type, assignedDataset);
		log.info("end of test assigning elementary flows");
		
	}
	
	@Test(priority=104)
	public void testProductFlows() throws InterruptedException {
		log.info("begin to test assigning product flows");
		String type = "flows";
		String assignedDataset = "ffd105a3-1ae2-4af3-b7b0-ebceb224fd40";
		
		assignAnonymous(type, assignedDataset);
		assignNotWrite(type, assignedDataset);
		wrongDataStockID(type, assignedDataset);
		wrongDatasetID(type, "3c0a1214-f4b2-4254-8fca-1d9e6ee9839f");
		rootDataStock(type, assignedDataset);
		wrongVersion(type, assignedDataset, "02.00.000");
		wrongVersionFormat(type, assignedDataset);
		wrongDependency(type, assignedDataset);
		
//		Due to a not fixed bug, the following four test methods cannot be run correctly.
//		assign(type, assignedDataset);
//		assignVersion(type, "bc0d4a49-8840-45dd-ab3e-e18928012306", "01.00.000");
		
//		Dependencies is currently not supported
//		String[] dependencies = {"16e9b2c6-3c44-11dd-ae16-0800200c9a66"};
//		String[] dependencyType = {"contacts"};
//		assignDependencies(type, "82b33e71-bfaa-49b4-9627-ee5963433f6e");
		
//		alreadyExisting(type, assignedDataset);
		log.info("end of test assigning product flows");
	}
	
	@Test(priority=105)
	public void testFlowPorperties() throws InterruptedException {
		log.info("begin to test assigning flow properties");
		String type = "flowproperties";
		String assignedDataset = "93a60a56-a3c8-19da-a746-0800200c9a66";
		
		assignAnonymous(type, assignedDataset);
		assignNotWrite(type, assignedDataset);
		wrongDataStockID(type, assignedDataset);
		wrongDatasetID(type, "3c0a1214-f4b2-4254-8fca-1d9e6ee9839f");
		rootDataStock(type, assignedDataset);
		wrongVersion(type, assignedDataset, "02.00.000");
		wrongVersionFormat(type, assignedDataset);
		wrongDependency(type, assignedDataset);
		

		assign(type, assignedDataset);
		assignVersion(type, "93a60a56-a3c8-14da-a746-0800200c9a66", "03.00.000");
		
		String[] dependencies = {"838aaa22-0117-11db-92e3-0800200c9a66"};
		String[] dependencyType = {"unitgroups"};
		assignDependencies(type, "838aaa23-0117-11db-92e3-0800200c9a66", dependencies, dependencyType);
		
		alreadyExisting(type, assignedDataset);
		log.info("end of test assigning flow properties");
		
	}
	
	@Test(priority=106)
	public void testSources() throws InterruptedException {
		log.info("begin to test assigning sources");
		String type = "sources";
		String assignedDataset = "dff34d61-54e9-4404-95ca-c09cc01b7f40";
		
		assignAnonymous(type, assignedDataset);
		assignNotWrite(type, assignedDataset);
		wrongDataStockID(type, assignedDataset);
		wrongDatasetID(type, "3c0a1214-f4b2-4254-8fca-1d9e6ee9839f");
		rootDataStock(type, assignedDataset);
		wrongVersion(type, assignedDataset, "02.00.000");
		wrongVersionFormat(type, assignedDataset);
		wrongDependency(type, assignedDataset);
		
		assign(type, assignedDataset);
		assignVersion(type, "8039a379-7a2b-4f51-a062-62b3cf1723ee", "03.00.000");
		
		String[] dependencies = {"d0f67f21-22d8-4e5e-914b-6f7dde923e33"};
		String[] dependencyType = {"contacts"};
		assignDependencies(type, "544205b8-bcc5-4850-a5b6-241434f23be6", dependencies, dependencyType);
		
		alreadyExisting(type, assignedDataset);
		log.info("end of test assigning sources");
		
	}
	
	@Test(priority=107)
	public void testUnitGroups() throws InterruptedException {
		log.info("begin to test assigning unit groups");
		String type = "unitgroups";
		String assignedDataset = "ff8ed45d-bbfb-4531-8c7b-9b95e52bd41d";
		
		assignAnonymous(type, assignedDataset);
		assignNotWrite(type, assignedDataset);
		wrongDataStockID(type, assignedDataset);
		wrongDatasetID(type, "3c0a1214-f4b2-4254-8fca-1d9e6ee9839f");
		rootDataStock(type, assignedDataset);
		wrongVersion(type, assignedDataset, "02.00.000");
		wrongVersionFormat(type, assignedDataset);
		wrongDependency(type, assignedDataset);
		
		assign(type, assignedDataset);
		assignVersion(type, "f170abd3-f010-45f4-8e7c-9871a5c0421b", "03.00.000");
		
//		Datastock does not have any dependencies in Unit Groups
//		String[] dependencies = {"16e9b2c6-3c44-11dd-ae16-0800200c9a66"};
//		String[] dependencyType = {"contacts"};
//		assignDependencies(type, "b4cac580-5ce8-11df-a08a-0800200c9a66");
		
		alreadyExisting(type, assignedDataset);
		log.info("end of test assigning unit groups");
		
	}
	
	@Test(priority=108)
	public void testContacts() throws InterruptedException {
		log.info("begin to test assigning contacts");
		String type = "contacts";
		
		String assignedDataset = "5bb337b0-9a1a-11da-a72b-0800200c9a69";
		
		assignAnonymous(type, assignedDataset);
		assignNotWrite(type, assignedDataset);
		wrongDataStockID(type, assignedDataset);
		wrongDatasetID(type, "3c0a1214-f4b2-4254-8fca-1d9e6ee9839f");
		rootDataStock(type, assignedDataset);
		wrongVersion(type, assignedDataset, "02.00.000");
		wrongVersionFormat(type, assignedDataset);
		wrongDependency(type, assignedDataset);
		
		assign(type, assignedDataset);
		assignVersion(type, "aa0a2faf-0bcf-428f-9fdf-c74d7e6a5136", "03.00.000");
		
//		Datastock does not have any dependencies on contacts
//		String[] dependencies = {"16e9b2c6-3c44-11dd-ae16-0800200c9a66"};
//		String[] dependencyType = {"contacts"};
//		assignDependencies(type, "d0f67f21-22d8-4e5e-914b-6f7dde923e33");
		
		alreadyExisting(type, assignedDataset);
		log.info("end of test assigning contacts");
		
	}
	

	/**
	 * Checks assigning a dataset as an anonymous user (not authenticated). 
	 * @param type The type of dataset to assign
	 * @param dataset_id The UUID of dataset to assign
	 */
	private void assignAnonymous(String type, String dataset_id) {
		log.debug("Assigning as anonymous user");

		TestFunctions.checkDatasetExisting(null, STOCK_ID, dataset_id, type, false, HEADER_NAME, client);
		
		if (log.isDebugEnabled()) {
			log.debug("PUT URI is: '" + TestContext.PRIMARY_SITE_URL + "resource/datastocks/"+ STOCK_ID + "/"+ type + "/" + dataset_id);
		}
		ClientResponse response = client.resource(TestContext.PRIMARY_SITE_URL + "resource/datastocks/"+ STOCK_ID + "/"+ type + "/" + dataset_id).put(ClientResponse.class);
		int status = response.getStatus();
		String responseString = response.getEntity(String.class);
		if (status != 401 || !responseString.equals("You are not permitted to operate on this data stock.")) {
			org.testng.Assert.fail("After trying to assign as anonymous user, the response status was either wrong (expected: 401, got: " + status + ") or an unexpected response message was sent (message: " + responseString + ")");	
		}
		
		TestFunctions.checkDatasetExisting(null, STOCK_ID, dataset_id, type, false, HEADER_NAME, client);
		
	}
	
	/**
	 * Checks assigning a dataset as user without write permissions
	 * @param type The type of dataset to assign
	 * @param dataset_id The UUID of dataset to assign
	 */
	private void assignNotWrite(String type, String dataset_id) {
		log.debug("Assigning as user without write permissions");
		
		String APIKey = TestFunctions.generateAPIKeyGET(client, READING_USER, PASSWORD, 200);
		if (log.isTraceEnabled()) {
			log.trace("API key is: " + APIKey);
		}
		
		TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dataset_id, type, false, HEADER_NAME, client);
		
		if (log.isDebugEnabled()) {
			log.debug("PUT URI is: '" + TestContext.PRIMARY_SITE_URL + "resource/datastocks/"+ STOCK_ID + "/"+ type + "/" + dataset_id);
		}
		ClientResponse response = client.resource(TestContext.PRIMARY_SITE_URL + "resource/datastocks/"+ STOCK_ID + "/"+ type + "/" + dataset_id).header(HEADER_NAME, "Bearer " + APIKey).put(ClientResponse.class);
		int status = response.getStatus();
		String responseString = response.getEntity(String.class);
		if (status != 401 || !responseString.equals("You are not permitted to operate on this data stock.")) {
			org.testng.Assert.fail("After trying to assign as user without writing permissions, the response status was either wrong (expected: 401, got: " + status + ") or an unexpected response message was sent (message: " + responseString + ")");
		}
		
		TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dataset_id, type, false, HEADER_NAME, client);
		
	}
	
	/**
	 * Checks assigning a plain dataset.
	 * @param type The type of dataset to assign
	 * @param dataset_id The UUID of dataset to assign
	 */
	private void assign(String type, String dataset_id) {
		log.debug("Assigning as user without any other parameters");
		
		String APIKey = TestFunctions.generateAPIKeyGET(client, WRITING_USER, PASSWORD, 200);
		
		TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dataset_id, type, false, HEADER_NAME, client);
		
		if (log.isDebugEnabled()) {
			log.debug("PUT URI is: '" + TestContext.PRIMARY_SITE_URL + "resource/datastocks/"+ STOCK_ID + "/"+ type + "/" + dataset_id);
		}
		ClientResponse response = client.resource(TestContext.PRIMARY_SITE_URL + "resource/datastocks/"+ STOCK_ID + "/"+ type + "/" + dataset_id).header(HEADER_NAME, "Bearer " + APIKey).put(ClientResponse.class);
		int status = response.getStatus();
		String responseString = response.getEntity(String.class);
		if (log.isDebugEnabled()) {
			log.debug("after assigning the status is: " + status);
			log.debug("after assigning the response message is: " + responseString);
		}
		if (status != 200 || !responseString.equals("Dataset has been assigned to data stock.")) {
			org.testng.Assert.fail("After trying to assign as user with the right permissions, the response status was either wrong (expected: 200, got: " + status + ") or an unexpected response message was sent (message: " + responseString + ")");
		}
		log.trace("API Key: " + APIKey);
		TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dataset_id, type, true, HEADER_NAME, client);
		
	}

	/**
	 * Checks assigning a dataset with a given version
	 * @param type The type of dataset to assign
	 * @param dataset_id The UUID of datset to assign
	 * @param version The (correct) version of dataset to assign
	 */
	private void assignVersion(String type, String dataset_id, String version) {
		log.debug("Assigning with version");
		
		String APIKey = TestFunctions.generateAPIKeyGET(client, WRITING_USER, PASSWORD, 200);
		
		TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dataset_id, type, false, HEADER_NAME, client);

		if (log.isDebugEnabled()) {
			log.debug("PUT URI is: '" + TestContext.PRIMARY_SITE_URL + "resource/datastocks/"+ STOCK_ID + "/" + type + "/" + dataset_id + "?version=" + version);
		}
		ClientResponse response = client.resource(TestContext.PRIMARY_SITE_URL + "resource/datastocks/"+ STOCK_ID + "/" + type + "/" + dataset_id + "?version=" + version).header(HEADER_NAME, "Bearer " + APIKey).put(ClientResponse.class);
		int status = response.getStatus();
		String responseString = response.getEntity(String.class);
		if (status != 200 || !responseString.equals("Dataset has been assigned to data stock.")) {
			org.testng.Assert.fail("After trying to assign with additional version parameter, the response status was either wrong (expected: 200, got: " + status + ") or an unexpected response message was sent (message: " + responseString + ")");
		}
		
		TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dataset_id, type, true, HEADER_NAME, client);
		
	}
	
	/**
	 * Checks assigning a dataset with all its dependencies
	 * @param type The type of dataset to assign
	 * @param dataset_id The UUID of dataset to assign
	 * @param dependencies The list of dependency UUIDs that should be assigned with
	 * @param dependencyType The types of dependencies rthat should be assigned with
	 */
	private void assignDependencies(String type, String dataset_id, String[] dependencies, String[] dependencyType) {
		log.debug("Assigning with dependencies");
		
		String APIKey = TestFunctions.generateAPIKeyGET(client, WRITING_USER, PASSWORD, 200);
		
		TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dataset_id, type, false, HEADER_NAME, client);
		
		for(int i = 0; i < dependencies.length; i++) {
			TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dependencies[i], dependencyType[i], false, HEADER_NAME, client);
		}
		
		if (log.isDebugEnabled()) {
			log.debug("PUT URI is: '" + TestContext.PRIMARY_SITE_URL + "resource/datastocks/" + STOCK_ID + "/" + type + "/" + dataset_id);
		}
		ClientResponse response = client.resource(TestContext.PRIMARY_SITE_URL + "resource/datastocks/"+ STOCK_ID + "/"+ type + "/" + dataset_id + "?withDependencies=" + correct_dependency).header(HEADER_NAME, "Bearer " + APIKey).put(ClientResponse.class);
		int status = response.getStatus();
		String responseString = response.getEntity(String.class);
		if (status != 200 || !responseString.equals("Dataset with its dependencies has been assigned to data stock.")) {
			org.testng.Assert.fail("After trying to assign as user with additional dependencies parameter, the response status was either wrong (expected: 200, got: " + status + ") or an unexpected response message was sent (message: " + responseString + ")");
		}
		
		TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dataset_id, type, true, HEADER_NAME, client);
		
		for(int i = 0; i < dependencies.length; i++) {
			TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dependencies[i], dependencyType[i], true, HEADER_NAME, client);
		}
		
	}
	
	/**
	 * Checks assigning a dataset to a not existing data stock.
	 * @param type The type of dataset to assing
	 * @param dataset_id The UUID of dataset to assing
	 */
	private void wrongDataStockID(String type, String dataset_id) {
		log.debug("Assigning with wrong data stock ID");
		String stock_id = "7ztzqpuhfdgfd7te89rw";
		
		String APIKey = TestFunctions.generateAPIKeyGET(client, WRITING_USER, PASSWORD, 200);
		
		TestFunctions.checkDatasetExisting(APIKey,stock_id,  dataset_id, type, false, HEADER_NAME, client);
		 if (log.isDebugEnabled()) {
			 log.debug("PUT URI is: '" + TestContext.PRIMARY_SITE_URL + "resource/datastocks/" + STOCK_ID + "/" + type + "/" + dataset_id);
		 }
		 ClientResponse response = client.resource(TestContext.PRIMARY_SITE_URL + "resource/datastocks/" + stock_id + "/" + type + "/" + dataset_id).header(HEADER_NAME, "Bearer " + APIKey).put(ClientResponse.class);
		int status = response.getStatus();
		String responseString = response.getEntity(String.class);
		if (status != 401 || !responseString.equals("Data stock is not correctly set.")) {
			org.testng.Assert.fail("After trying to assign dataset to not exisiting data stock, the response status was either wrong (expected: 401, got: " + status + ") or an unexpected response message was sent (message: " + responseString + ")");
		}
		
		TestFunctions.checkDatasetExisting(APIKey, stock_id, dataset_id, type, false, HEADER_NAME, client);
		
	}
	
	/**
	 * Checks assigning a dataset that is not of given type or does not exist in datastock.
	 * @param type The (wrong) type of dataset to assign
	 * @param dataset_id The UUID of dataset to assign
	 */
	private void wrongDatasetID(String type, String dataset_id) {
		log.debug("Assigning not existing dataset");
		
		String APIKey = TestFunctions.generateAPIKeyGET(client, WRITING_USER, PASSWORD, 200);
		
		TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dataset_id, type, false, HEADER_NAME, client);
		
		if (log.isDebugEnabled()) {
			log.debug("PUT URI is: '" + TestContext.PRIMARY_SITE_URL + "resource/datastocks/" + STOCK_ID + "/" + type + "/" + dataset_id);
		}
		ClientResponse response = client.resource(TestContext.PRIMARY_SITE_URL + "resource/datastocks/" + STOCK_ID + "/" + type + "/" + dataset_id).header(HEADER_NAME, "Bearer " + APIKey).put(ClientResponse.class);
		int status = response.getStatus();
		String responseString = response.getEntity(String.class);
		if (status != 401 || !responseString.equals("Dataset with given UUID is not existing in database.")) {
			org.testng.Assert.fail("After trying to assign wrong dataset to "+ STOCK_ID + ", the response status was either wrong (expected: 401, got: " + status + ") or an unexpected response message was sent (message: " + responseString + ")");
		}
		
		TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dataset_id, type, false, HEADER_NAME, client);
		
	}
	
	/**
	 * Checking whether a dataset can be assigned to a root data stock.
	 * @param type The type of the dataset to assign
	 * @param dataset_id The UUID of dataset to assing
	 */
	private void rootDataStock(String type, String dataset_id) {
		log.debug("Assigning to root data stock");
		
		String APIKey = TestFunctions.generateAPIKeyGET(client, "Admin1", PASSWORD, 200);
		
		TestFunctions.checkDatasetExisting(APIKey,ROOT_STOCK_ID, dataset_id, type, false, HEADER_NAME, client);
		
		if (log.isDebugEnabled()) {
			log.debug("PUT URI is: '" + TestContext.PRIMARY_SITE_URL + "resource/datastocks/" + STOCK_ID + "/" + type + "/" + dataset_id);
		}
		ClientResponse response = client.resource(TestContext.PRIMARY_SITE_URL + "resource/datastocks/" + ROOT_STOCK_ID + "/"+ type + "/" + dataset_id).header(HEADER_NAME, "Bearer " + APIKey).put(ClientResponse.class);
		int status = response.getStatus();
		String responseString = response.getEntity(String.class);
		if (status != 401 || !responseString.equals("Data stock must be a logical data stock.")) {
			org.testng.Assert.fail("After trying to assign dataset to root data stock, the response status was either wrong (expected: 401, got: " + status + ") or an unexpected response message was sent (message: " + responseString + ")");
		}
		
		TestFunctions.checkDatasetExisting(APIKey, ROOT_STOCK_ID, dataset_id, type, false, HEADER_NAME, client);
		
	}

	/**
	 * Checks assigning a dataset with wrong version number.
	 * @param type The type of dataset to assign
	 * @param dataset_id The UUID of dataset to assign
	 * @param version An (incorrect) version of dataset
	 */
	private void wrongVersion(String type, String dataset_id, String version) {
		log.debug("Assigning with wrong version");
		
		String APIKey = TestFunctions.generateAPIKeyGET(client, WRITING_USER, PASSWORD, 200);
		
		TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dataset_id, type, false, HEADER_NAME, client);
		
		if (log.isDebugEnabled()) {
			log.debug("PUT URI is: '" + TestContext.PRIMARY_SITE_URL + "resource/datastocks/" + STOCK_ID + "/"+ type + "/" + dataset_id + "?version="+ version);
		}
		ClientResponse response = client.resource(TestContext.PRIMARY_SITE_URL + "resource/datastocks/" + STOCK_ID + "/"+ type + "/" + dataset_id + "?version="+ version).header(HEADER_NAME, "Bearer " + APIKey).put(ClientResponse.class);
		int status = response.getStatus();
		String responseString = response.getEntity(String.class);
		if (status != 401 || !responseString.equals("Dataset with given UUID and version is not existing in database.")) {
			org.testng.Assert.fail("After trying to assign dataset with incorrect version to "+ STOCK_ID + ", the response status was either wrong (expected: 401, got: " + status + ") or an unexpected response message was sent (message: '" + responseString + "')");
		}
		
		TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dataset_id, type, false, HEADER_NAME, client);
		
	}
	
	/**
	 * Checks assigning a dataset with a version in wrong format.
	 * @param type The type of dataset to assign
	 * @param dataset_id The UUID of dataset to assign
	 */
	private void wrongVersionFormat (String type, String dataset_id) {
		log.debug("Assigning with wrong version format");
		
		String APIKey = TestFunctions.generateAPIKeyGET(client, WRITING_USER, PASSWORD, 200);
		
		TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dataset_id, type, false, HEADER_NAME, client);
		
		if (log.isDebugEnabled()) {
			log.debug("PUT URI is: '" + TestContext.PRIMARY_SITE_URL + "resource/datastocks/" + STOCK_ID + "/" + type + "/" + dataset_id+ "?version=" + WRONG_VERION_FORMAT);
		}
		ClientResponse response = client.resource(TestContext.PRIMARY_SITE_URL + "resource/datastocks/" + STOCK_ID + "/" + type + "/" + dataset_id+ "?version=" + WRONG_VERION_FORMAT).header(HEADER_NAME, "Bearer " + APIKey).put(ClientResponse.class);
		int status = response.getStatus();
		String responseString = response.getEntity(String.class);
		if (status != 401 || !responseString.equals("Version is not in a correct format.")) {
			org.testng.Assert.fail("After trying to assign dataset to not exisiting data stock, the response status was either wrong (expected: 401, got: " + status + ") or an unexpected response message was sent (message: " + responseString + ")");
		}
		
		TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dataset_id, type, false, HEADER_NAME, client);
		
	}
	
	/**
	 * Checks assigning a dataset with not existing dependency mode.
	 * @param type The type of dataset to assign
	 * @param dataset_id Te UUID of dataset to assign
	 */
	private void wrongDependency(String type, String dataset_id) {
		log.debug("Assigning with wrong dependency");
		
		String APIKey = TestFunctions.generateAPIKeyGET(client, WRITING_USER, PASSWORD, 200);
		
		TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dataset_id, type, false, HEADER_NAME, client);
		
		if (log.isDebugEnabled()) {
		
			log.debug("PUT URI is: '" + TestContext.PRIMARY_SITE_URL + "resource/datastocks/" + STOCK_ID + "/" + type + "/" + dataset_id + "?withDependencies=" + wrong_dependency);
		}
		ClientResponse response = client.resource(TestContext.PRIMARY_SITE_URL + "resource/datastocks/" + STOCK_ID + "/" + type + "/" + dataset_id + "?withDependencies=" + wrong_dependency).header(HEADER_NAME, "Bearer " + APIKey).put(ClientResponse.class);
		
		
		int status = response.getStatus();
		String responseString = response.getEntity(String.class);
		if (status != 401 || !responseString.equals("Dependencies mode is not legal.")) {
			org.testng.Assert.fail("After trying to assign dataset to " + STOCK_ID + " with wrong dependencies, the response status was either wrong (expected: 401, got: " + status + ") or an unexpected response message was sent (message: " + responseString + ")");
		}
		
		TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dataset_id, type, false, HEADER_NAME, client);
		
	}
	
	/**
	 * Checks an assigning an already assigned dataset.
	 * @param type The type of dataset to assign
	 * @param dataset_id The UUID of dataset to assign
	 */
	private void alreadyExisting(String type, String dataset_id) {
		log.debug("Assigning already existing dataset");
		
		String APIKey = TestFunctions.generateAPIKeyGET(client, WRITING_USER, PASSWORD, 200);
		
		TestFunctions.checkDatasetExisting(APIKey, STOCK_ID, dataset_id, type, true, HEADER_NAME, client);
		
		if (log.isDebugEnabled()) {
			log.debug("PUT URI is: '" + TestContext.PRIMARY_SITE_URL + "resource/datastocks/" + STOCK_ID + "/" + type + "/" + dataset_id);
		}
		ClientResponse response = client.resource(TestContext.PRIMARY_SITE_URL + "resource/datastocks/" + STOCK_ID + "/" + type + "/" + dataset_id).header(HEADER_NAME, "Bearer " + APIKey).put(ClientResponse.class);
		int status = response.getStatus();
		String responseString = response.getEntity(String.class);
		if (status != 200 || !responseString.equals("Dataset is already assigned to data stock.")) {
			org.testng.Assert.fail("After trying to assign already existing to "+ STOCK_ID + ", the response status was either wrong (expected: 200, got: " + status + ") or an unexpected response message was sent (message: " + responseString + ")");
		}
		
	}
			

}
