package edu.kit.soda4lca.test.ui.main;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.xml.bind.JAXBElement;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetList;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetVO;
import de.iai.ilcd.xml.read.JAXBContextResolver;

/**
 * Functions, that are used in different testcases, like login
 * 
 * @author mark.szabo
 * 
 */
public class TestFunctions {

	protected final static Logger log = org.apache.log4j.Logger.getLogger(TestFunctions.class);

	/**
	 * Checks if an element is on the site, and return true if it is, and false if isn't
	 * 
	 * @param element
	 * @return
	 */
	public static boolean isElementNotPresent( By element ) {
		try {
			TestContext.getInstance().getDriver().findElement( element );
			return true;
		}
		catch ( Exception e ) {
			return false;
		}
	}
	
	/**
	 * Login on the side
	 * 
	 * @param user
	 *            username
	 * @param password
	 *            password
	 * @param correct
	 *            should be true, if the user/pass is a valid logindata
	 * @param staylogedin
	 *            stay logged in or logout after checking the login
	 * @param testadmin
	 *            Integer. 0 - no need to test the admin rights, 1 - test the admin rights and it should be admin, 2 -
	 *            test the admin rights and it should NOT be admin
	 */
	public static void login( String user, String password, Boolean correct, Boolean staylogedin, Integer testadmin, Boolean remembered) {
		// 'correct' should be true, if the user/pass is a valid logindata
		login(user, password, correct, staylogedin, testadmin, remembered, TestContext.PRIMARY_SITE_URL, null);
	} 
	
	public static void loginWithExpiredPassword( String user, String password, String newPassword, Boolean correct, Boolean staylogedin, Integer testadmin, Boolean remembered) {
		login(user, password, correct, staylogedin, testadmin, remembered, TestContext.PRIMARY_SITE_URL, newPassword);
	}

	/**
	 * Login on the side
	 * 
	 * @param user
	 *            username
	 * @param password
	 *            password
	 * @param correct
	 *            should be true, if the user/pass is a valid logindata
	 * @param staylogedin
	 *            stay logged in or logout after checking the login
	 * @param testadmin
	 *            Integer. 0 - no need to test the admin rights, 1 - test the admin rights and it should be admin, 2 -
	 *            test the admin rights and it should NOT be admin
	 * @param site The URL string of site to connect to           
	 */
	public static void login( String user, String password, Boolean correct, Boolean staylogedin, Integer testadmin, Boolean remembered, String site, String newPassword) 
	// 'correct' should be true, if the user/pass is a valid logindata
	{
		WebDriverRunner.setWebDriver(TestContext.getInstance().getDriver());
		// open the main site
        WebDriverRunner.clearBrowserCache();
        open(site);

		// click login
		$x(".//*[@id='leftFooter']/a[contains(@href, 'login.xhtml')]").click();
		
		loginOnLoginSite(user, password, remembered);
		
		if (newPassword != null) {
			changeExpiredPasswordOnSite(newPassword);
		}
		
		if (correct) {
			// test the admin rights, if need
			if (testadmin == 1)// the user is admin
			{
				// locate 'Admin area' button
				$(By.linkText(TestContext.lang.getProperty( "admin.adminArea"))).shouldBe(visible);
			}
			else if ( testadmin == 2 ) {
				if ($(By.linkText(TestContext.lang.getProperty( "admin.adminArea"))).exists())
					org.testng.Assert.fail("The user should not be able to see the 'Admin area' link, but he sees");
			}
			if ($x(".//*[text()='" + TestContext.lang.getProperty("common.privacyPolicyTitle")+ "']").exists()) {
				$(By.id("acceptPrivacyPolicy")).click();
				TestFunctions.clickButtonWithI18nLabel("admin.continue");
			}
			// locate Logout link and logout .//*[@id='leftFooter']/a[2] and the next site .//*[@id='logoutButton']
			if ( staylogedin ) {
				// wait
				$x(".//*[@id='leftFooter']/a[3]").shouldBe(visible);
			}
			else // logout if staylogedin is false
			{
				logout();
			}
		}
		else {
			// locate 'Incorrect user name/password' label .//*[@id='messages']/div
			// wait
			$x(".//*[@id='messages']/div").waitUntil(visible, TestContext.timeout);
		}
		
	}
	/**
	 * Logs directly in with given parameters without any tests about correctness of user/password combination.
	 * @param user 
	 * 			the user name 
	 * @param password 
	 * 				the password
	 * @param remembered 
	 * 					Should be true if user has clicked remember me check box 
	 * @param driver 
	 * 				The given WebDriver that 
	 */
	public static void loginOnLoginSite( String user, String password, Boolean remembered) {
		// login
		// wait for site to load
		$x(".//*[@id='loginButton']").shouldBe();
		// fill in the login form and click Login
		$x(".//input[@id='name']").clear();
		$x(".//input[@id='name']").setValue(user);
		$x(".//input[@id='passw']").clear();
		$x(".//input[@id='passw']").setValue(password);
		if (remembered) {
			$x(".//*[@id='rememberme']").click();
		}
		$x(".//*[@id='loginButton']").click();
	}
	
	/**
	 * Changes an expired password
	 * @param password
	 * 			the new password to be set
	 * @param driver
	 * 			the given WebDriver that
	 */
	public static void changeExpiredPasswordOnSite(String password) {
		$x(".//*[@id='changePasswordButton']").shouldBe(visible);
		$x(".//input[@id='passw']").clear();
		$x(".//input[@id='passw']").setValue(password);	
		$x(".//input[@id='repeatPassw']").clear();
		$x(".//input[@id='repeatPassw']").setValue(password);
		$x(".//*[@id='changePasswordButton']").click();
	}

	public static void login( String user, String password, Boolean correct ) {
		login( user, password, correct, false, 0 );
	}

	public static void login( String user, String password, Boolean correct, Boolean staylogedin ) {
		login( user, password, correct, staylogedin, 0 );
	}
	
	public static void login( String user, String password, Boolean correct, Boolean staylogedin, Integer testadmin )
	{
		login(user, password, correct, staylogedin, testadmin, false);
	}
	
	public static void loginWithExpiredPassword( String user, String password, String newPassword, Boolean correct, Boolean staylogedin )
	{
		loginWithExpiredPassword(user, password, newPassword, correct, staylogedin, 0, false);
		
	}
		
	/**
	 * Logs user out.
	 * 
	 */
	public static void logout() {
		// wait
		String logoutNumber;
		if ($(By.id("admin_content")).exists()) {
			logoutNumber = "4";
		} else {
			logoutNumber = "3";
		}
		$x(".//*[@id='leftFooter']/a[" + logoutNumber + "]").click();
		// wait
		$x(".//*[@id='logoutButton']").click();
	}
	
	
	
	public static void gotoAdminArea() {
        TestFunctions.findAndWaitOnElement( By.linkText( TestContext.lang.getProperty( "admin.adminArea" ) ) ).click();
        
        TestFunctions.waitOnAdminArea();
    }

    public static void waitOnAdminArea() {
        $(By.linkText( TestContext.lang.getProperty( "admin.globalConfig" ) ) ).shouldBe(enabled);
    }

	public static Properties propOpen( String language ) {

		// language properties
		Properties lang = new Properties();
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		try {
			if ( language == "en" ) // for English is the properties file in ISO8859-1
			{
				lang.load( loader.getResourceAsStream( "resources/lang_" + language + ".properties" ) );
			}
			else // for german it's in UTF-8
			{
				URL resource = loader.getResource( "resources/lang_" + language + ".properties" );
				lang.load( new InputStreamReader( resource.openStream(), "UTF8" ) );
			}
		}
		catch ( IOException e ) {
			System.out.println( e.getMessage() );
			return null;
		}

		return lang;
	}

	/**
	 * Sometimes the UI too slow because of client side scripts. Then we have to wait for an element, before click on it
	 * 
	 * @param b
	 * @return
	 */
	public static WebElement findAndWaitOnElement( By b ) {
		return findAndWaitOnElement(b, TestContext.timeout);
	}

	/**
	 * Sometimes the UI too slow because of client side scripts. Then we have to wait for an element, before click on it
	 * 
	 * @param b
	 * @param timeout
	 * @return
	 */
	public static WebElement findAndWaitOnElement( By b, int timeout ) {
	    for (int i = 0; i < timeout * 2; i++) {
	        if ($(b).isDisplayed())
	            break;
	    }
	    		
		// then select
		return $(b);
	}
	
	/**
	 * Sometimes the UI too slow because of client side scripts. Then we have to wait for an element, before click on it
	 * 
	 * @param b
	 * @return
	 */
	public static WebElement findAndWaitOnConsoleElement( By b ) {
		// first wait
		(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( b ) );
	
		WebElement e = TestContext.getInstance().getDriver().findElement(b);
	
		// we need to scroll there first because Selenium doesn't
		int elementPosition = e.getLocation().getY();
		String js = String.format("window.scroll(0, %s)", elementPosition);
		((JavascriptExecutor)TestContext.getInstance().getDriver()).executeScript(js);
		
		e = TestContext.getInstance().getDriver().findElement(b);

		e = TestContext.getInstance().getDriver().findElement(b);
		
		// then select
		return e;
	}


	/**
	 * Find a check box (PrimeFaces implementation specific)
	 * 
	 * @param b
	 * @return
	 */
	public static WebElement findAndWaitOnCheckBox( String label ) {
		return $x( ( "//span[@class='ui-chkbox-label'][text()='" + label + "']" ) );
	}

	/**
	 * find a button in a dialog with a given title (PrimeFaces implementation specific)
	 * 
	 * @return
	 */
	public static WebElement findDialogButton( String dialogTitle, String buttonCaption ) {
		return TestContext.getInstance().getDriver().findElement(By.xpath(".//button[preceding::div[child::span[text()='" + dialogTitle + "']] and child::span[text()='" + buttonCaption + "']]"));
	}

	/**
	 * Sometimes the correct behavior is, that no error message shows up. This function test the message and drop an
	 * org.testng.Assert.fail if any message showed up
	 */
	public static void checkNoMessage() {
		// check if there is no message
		try {
			$x( ".//*[@id='messages']" );
		}
		catch ( Exception e ) {
			return;
		}

		// if there is a message, check if it's empty
		if ( $x( ".//*[@id='messages']" ).getText().isEmpty() )
			return;

		org.testng.Assert.fail( "Message appears: " + $x( ".//*[@id='messages']" ).getText() );
	}

	// Create an instance of HttpClient.
	public static HttpClient hclient = new HttpClient();

	/**
	 * For testing the service API it's necessary to have a function which gives back the source of the requested url as
	 * a string
	 * 
	 * @param url
	 *            End of the url after Main.site + "resource/" OR the full url, depending on the fullurl variable
	 * @param fullurl
	 *            decide whether 'url' is a full url or a relative path
	 *            if true
	 * @return
	 *         Returns the requested site's source
	 * @throws Exception
	 */
	public static String getUrl( String url, boolean fullurl ) throws Exception {
		if ( !fullurl )
			url = TestContext.PRIMARY_SITE_URL + "resource/" + url;

		System.out.println( "GETting " + url );
		// Create a method instance.
		GetMethod method = new GetMethod( url );

		// Provide custom retry handler is necessary
		method.getParams().setParameter( HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler( 3, false ) );

		String responseBody = null;

		try {
			// Execute the method.
			int statusCode = hclient.executeMethod( method );

			if ( statusCode != HttpStatus.SC_OK ) {
				System.err.println( "Method '" + method.getURI() + "' failed: " + method.getStatusLine() );
			}

			// Read the response body.

			// get respond length
			int length = method.getResponseBodyAsStream().available();
			if ( length < 1 ) // if it's too long, it returns with a 0, in this case we have to manually attach it to 10
							  // million
				length = 10000000;
			byte[] responseBodyByte = new byte[length];

			method.getResponseBodyAsStream().read( responseBodyByte, 0, length );

			responseBody = "";
			for ( int i = 0; i < length && responseBodyByte[i] > 0; i++ )
				responseBody += (char) responseBodyByte[i];

		}
		catch ( HttpException e ) {
			System.err.println( "Fatal protocol violation: " + e.getMessage() );
			e.printStackTrace();
		}
		catch ( IOException e ) {
			System.err.println( "Fatal transport error: " + e.getMessage() );
			e.printStackTrace();
		}
		finally {
			// Release the connection.
			method.releaseConnection();
		}
		return responseBody;
	}

	public static String getUrl( String urlend ) throws Exception {
		return getUrl( urlend, false );
	}
	
	/**
	 * Selects the given data stock.
	 * @param dataStock The data stock which shall be selected
	 */
	public static void selectDataStock(String dataStock) {
		findAndWaitOnElement(By.xpath(".//div[contains(@id, 'selectDataStock')]")).click();
		findAndWaitOnElement(By.xpath(".//li[text()='" + dataStock + "']")).click();
	}

	/**
	 * Waits until site is loaded
	 */
	public static void waitUntilSiteLoaded() {
		Selenide.sleep(TestContext.wait * 2);
		new WebDriverWait(TestContext.getInstance().getDriver(), TestContext.timeout).until(
			      webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
	}
	
	/**
	 * Hovers over given menu item to then click on given menu selection in menu item.
	 */
	public static void selectMenuItem(String menuItem, String menuSelection) {
		// Hover over the menu
		$( By.linkText( TestContext.lang.getProperty( menuItem ) ) ).hover();
		// Click on selection
		$( By.linkText( TestContext.lang.getProperty( menuSelection ) ) ).click();
	}
	
	/**
	 * Selects given data set.
	 * @param dataSet The data set which shall be selected.
	 */
	public static void selectDataSet(String dataSet) {
		// Hover over the menu
		waitUntilSiteLoaded();
		$( By.linkText( TestContext.lang.getProperty( "admin.dataset.manageList" ) ) ).hover();
		// Click on data set 
		String option = "";
		if (dataSet.endsWith("es")) {
			if (dataSet.endsWith("ies")) {
				option = dataSet.replace("ies", "y");
			} else if (dataSet == "sources") {
				option = "source";
			} else {
				option = dataSet.substring(0, dataSet.length() - 2);
			}
			
		} else if (dataSet.endsWith("s")) {
			option = dataSet.substring(0, dataSet.length() - 1);
		}
		$( By.linkText( TestContext.lang.getProperty( "admin." + option + ".manageList" ) ) ).click();
	}
	
	/**
	 * Counts the total number of users
	 */
	public static int countTotalUsersEntries() {
		return countTotalTableEntries(null, false);
	}
	
	/**
	 * Counts the total number entries in data table.
	 * @param type The data set type, if needed
	 * @param assignView The flag indicating whether the table is in Assign Dataset View
	 * @return The number of total table entries
	 */
	public static int countTotalTableEntries(String type, boolean assignView) {
		String paginatorXPath = ".//div[contains(@id, '_paginator_top')]";
		if (assignView) {
			paginatorXPath = ".//div[contains(@id, '"+ type + "')]/div[contains(@id, '_paginator_top')]";
		}
		String currentElementText = TestFunctions.findAndWaitOnElement(By.xpath(paginatorXPath)).getText();
		String tempString = StringUtils.substringAfter(currentElementText, " (");
		String numberTempString = StringUtils.substringBefore(tempString, " " + TestContext.lang.getProperty("common.list.total"));
		int usersEntries = Integer.valueOf(numberTempString);
		return usersEntries;
	}

	/**
	 * Counts the total number of entries in a data set.
	 * @return the total amount of entries in a data set.
	 */
	public static int countTotalDatasetEntries() {
		String currentElementText = findAndWaitOnElement(By.className("ui-paginator-current")).getText();
		String tempString = StringUtils.substringBefore(currentElementText, " (");
		String numberTempString = StringUtils.substringAfter(tempString, ": ");
		int datasetEntries = Integer.valueOf(numberTempString);
		return datasetEntries;
	} 
	
	/**
	 * Counts all data sets which are visible on the current page of a data set.
	 * @return The number of data sets visible on current page of data set.
	 */
	public static int countDatasetEntriesOnPage() {
		Select select = new Select(TestFunctions.findAndWaitOnElement(By.xpath(".//div[contains(@id,'_paginator_top')]/select[1]")));
		WebElement selectedOption = select.getFirstSelectedOption();
		String pageNumberString = selectedOption.getText();
		int pageEntries = Integer.valueOf(pageNumberString);
		return pageEntries;
	}

	/**
	 * Fills in inputText (given by its ID) with given String. Uses the findAndWaitOnElement without Selenide for stability reasons.
	 * @param inputTextId The ID of to be filled in intputText
	 * @param content The content for inputText
	 */
	public static void fillInputText(String inputTextId, String content) {
		$x( ".//input[@id='" + inputTextId + "']" ).doubleClick();
		$x( ".//input[@id='" + inputTextId + "']" ).clear();
		if (content != null)
		$x( ".//input[@id='" + inputTextId + "']" ).sendKeys( content );
	}
	
	/**
	 * Clicks button with given label as i18n string. Uses findAndWaitOnConsoleElement without Selenide for stability reasons.
	 * @param name The i18n key for button label
	 */
	public static void clickButtonWithI18nLabel(String name) {
        findAndWaitOnConsoleElement(By.xpath(".//button/span[text()='"+ TestContext.lang.getProperty(name) +"']/..")).click();
    }
	
	/**
	 * Clicks the button with given name as label
	 * @param name The label name of button
	 */
	public static void clickButtonWithLabel(String name) {
		findAndWaitOnElement(By.xpath(".//button/span[text()='"+ name +"']/..")).click();
	}
	
	/**
	 * Clicks button that contains given i18n String als label.
	 */
	public static void clickButtonContainingI18nText(String name) {
		findAndWaitOnElement(By.xpath(".//button/span[contains(.,'"+ TestContext.lang.getProperty(name) +"')]/..")).click();
	} 
	
	/**
	 * Selects a given item of a oneSelectMenu given by its ID. Uses the findAndWaitOnElement without Selenide for stability reasons.
	 * @param menu The ID of oneSelectMenu
	 * @param item The to be selected item
	 */
	public static void selectItemInSelectBox(String selectBoxID, String item) {
		$(By.id(selectBoxID)).click();
		$x(".//li[text()='" + item + "']").waitUntil(visible, TestContext.wait * 2).click();
	}
	
	/**
	 * Selects all current visible entries of table with given ID.
	 * @param tableID The jsf ID of table
	 */
	public static void selectAllTableEntries(String tableID) {
		TestFunctions.findAndWaitOnElement(By.xpath(".//thead[@id='"+tableID+"_head']/tr[1]/th[1]/div[1]/div[2]")).click();
	}
		
	/**
	 * Goes to certain page via hovering over the admin menu.
	 * @param action The element that performs hovering and clicking
	 * @param menuList The list of menu items Selenium shall hover over; last item will be clicked
	 */
	public static void goToPageByAdminMenu(String... menuList) {
		for (int i = 0; i < menuList.length - 1; i++) {
			for (int j=0; j<TestContext.timeout && !$( By.linkText( TestContext.lang.getProperty( menuList[i + 1] ) ) ).isDisplayed(); j++) {
				$( By.linkText( TestContext.lang.getProperty( menuList[i] ) ) ).hover();
				waitUntilSiteLoaded();
			}
		}
		for (int i=0; i<TestContext.timeout && $(By.linkText( TestContext.lang.getProperty( menuList[menuList.length - 1] ) ) ).exists(); i++) {
			$( By.linkText( TestContext.lang.getProperty( menuList[menuList.length - 1] ) ) ).click();
			waitUntilSiteLoaded();
		}
	}
	
		/**
	 * Checks if table with given name in target instance has given number of data set entries.
	 * @param tableName name of table 
	 * @param number The expected number of daa set entries
	 */
	public static void targetTableContainsNumberDataSets(String tableName, Long number) {
		if (!(TestFunctions.findAndWaitOnElement(By.xpath(".//*[@id='" + tableName + "_paginator_top']/span")).getText().contains("(" + number))) {
			org.testng.Assert.fail("More or less " + tableName + "es were transferred to target instance.");
		}
	}	
	
	/**
	 * Checks if jsf element with given ID equals given text. Uses findAndWaitOnElement without Selenide for stability reasons.
	 * @param id The ID of to be checked jsf element
	 * @param toEqual The text the element is expected to equal
	 * @return True if element equals given text
	 */
	public static boolean elementWithIdEquals(String id, String toEqual) {
		return $(By.id(id)).getText().equals(toEqual);
	}
		
	/**
	 * Checks if table with given name is empty.
	 * @param tableName The name of to be checked table
	 * @return True if table contains no entries
	 */
	public static boolean isTableEmpty(String tableName) {
		return findAndWaitOnElement(By.xpath(".//*[@id='"+tableName+"_data']/tr[1]/td[1]")).getText()
					.equals(TestContext.lang.getProperty("common.list.noneFound"));
	}
		
	/**
	 * Checks if message with given language key is shown.
	 * @param i18nKey The language key
	 * @return True if message with given language key is shown
	 */
	public static boolean isMessageShown(String i18nKey) {
		for (int i=0; i < 500; i++) {
			if (isElementNotPresent(By.xpath(".//*[text()='" + TestContext.lang.getProperty(i18nKey) + "']")))
				return true;
		}
		return false;
	}
	
	public static String getMessage() {
		return findAndWaitOnElement(By.xpath(".//div[@id='messages']/div")).getText();
	}
	
	/**
	 * Checks if given text is shown.
	 * @param text The text that should be present
	 * @return True if given text is present
	 */
	public static boolean isTextShown(String text) {
		return isElementNotPresent(By.xpath(".//*[text()='" + text + "']"));
	}
		
	/**
	 * Checks if last entry of table with given name at given column equals the given expected text
	 * @param tableName The name of table
	 * @param column The wanted column
	 * @param toEqual The text the entry at given column is expected
	 * @return True if given text equals the content of given column in last table row
	 */
	public static boolean isLastTableEntryAtColumnEqual(String tableName, Long column, String toEqual) {
		return findAndWaitOnElement(By.xpath(".//*[@id='"+tableName+"_data']/tr[last()]/td["+column+"]")).getText().equals(toEqual);
	}
		
	/**
	 * Gets entry of given column in last table row with given table name.
	 * @param tableName The name of table
	 * @param column The wanted column
	 * @return A String containing the column  content of last table row
	 */
	public static String getLastTableEntryAtColumn(String tableName, Long column) {
		return TestFunctions.findAndWaitOnElement(By.xpath(".//*[@id='"+ tableName + "_data']/tr[last()]/td[" + column + "]")).getText();
	}
	
	/**
	 * Gets entry of given column in first row with given table name.
	 * @param tableName The name of table
	 * @param column The wanted column
	 * @return A String containing the column  content of last table row
	 */
	public static String getFirstTableEntryAtColumn(String tableName, Long column) {
		return TestFunctions.findAndWaitOnElement(By.xpath(".//*[@id='"+ tableName + "_data']/tr[1]/td[" + column + "]")).getText();
	}
	
	/**
	 * Gets entry of given column in second row with given table name.
	 * @param tableName The name of table
	 * @param column The wanted column
	 * @return A String containing the column  content of last table row
	 */
	public static String getSecondTableEntryAtColumn(String tableName, Long column) {
		return TestFunctions.findAndWaitOnElement(By.xpath(".//*[@id='"+ tableName + "_data']/tr[2]/td[" + column + "]")).getText();
	}
	
	/**
	 * Selects a table entry with given entry name in given column of given Table ID.
	 * @param tableID The jsf ID of given table
	 * @param name The id/name of table entry
	 * @param The column number the name is expected to be.
	 */
	public static void selectTableEntry(String tableID, String name, int columnNumber) {
		String xpath = ".//*[@id='"+tableID+ "_data']/tr/td[" + columnNumber + "]//*[text()='" + name + "']/../td[1]/div[1]/div[2]";
		if (isElementNotPresent(By.xpath(xpath)))
			findAndWaitOnElement(By.xpath(xpath)).click();
		else 
			findAndWaitOnElement(By.xpath(".//*[@id='"+tableID+ "_data']/tr/td["+ columnNumber + "]//*[text()='" + name + "']/../../td[1]/div[1]/div[2]")).click();
		
	}
	
	/**
	 * Selects a table entry with given row number in table with given table ID.
	 * @param tableID The jsf ID of given table
	 * @param rowNumber The wanted row number of to selected entry
	 */
	public static void selectTableEntryByNumber(String tableID, int rowNumber) {
		By by = By.xpath(".//*[@id='"+tableID+ "_data']/tr[" + rowNumber + "]/td[1]/div[1]/div[2]");
		findAndWaitOnElement(by).click();
		
	}
	
	/**
	 * Selets a table entry with given expected content in given column of table with given table ID.
	 * @param tableID The jsf table ID
	 * @param content The expected content
	 * @param columnNumber The column number the content sis xpected to be
	 */
	public static void selectTableEntryByContent(String tableID, String content, int columnNumber) {
		int i = 1;
		String xpath = ".//*[@id='"+tableID+ "_data']/tr[" + i+ "]/td[" + columnNumber + "]";
		if (findAndWaitOnElement(By.xpath(xpath)).getText() == null)
			xpath = ".//*[@id='"+tableID+ "_data']/tr["+ i + "]/td[" + columnNumber+ "]//*";
		while ( !TestFunctions.findAndWaitOnElement( By.xpath( xpath ) ).getText().equals(
				content ) ) {
			i++;
			xpath = ".//*[@id='"+tableID+ "_data']/tr[" + i+ "]/td[" + columnNumber + "]";
			if (findAndWaitOnElement(By.xpath(xpath)).getText() == null)
				xpath = ".//*[@id='"+tableID+ "_data']/tr["+ i + "]/td[" + columnNumber+ "]//*";
			
		}
		By by = By.xpath(".//*[@id='"+tableID+ "_data']/tr[" + i + "]/td[1]/div[1]/div[2]");
		findAndWaitOnElement(by).click();
		
	}
	
	/**
	 * Selets a table entry with given expected content in given column of table with given table ID.
	 * @param tableID The jsf table ID
	 * @param content The expected content
	 * @param columnNumber The column number the content is expected to be
	 * @param targetColumnNumber The column number of which content is wanted
	 * @param columnContent the wanted column content
	 */
	public static By getColumnTableEntryByContent(String tableID, String content, int columnNumber, int targetColumnNumber, String columnContent) {
		int i = 1;
		String xpath = ".//*[@id='"+tableID+ "_data']/tr[" + i+ "]/td[" + columnNumber + "]";
		if (findAndWaitOnElement(By.xpath(xpath)).getText() == null)
			xpath = ".//*[@id='"+tableID+ "_data']/tr["+ i + "]/td[" + columnNumber+ "]//*";
		while ( !TestFunctions.findAndWaitOnElement( By.xpath( xpath ) ).getText().equals(
				content ) ) {
			i++;
			xpath = ".//*[@id='"+tableID+ "_data']/tr[" + i+ "]/td[" + columnNumber + "]";
			if (findAndWaitOnElement(By.xpath(xpath)).getText() == null)
				xpath = ".//*[@id='"+tableID+ "_data']/tr["+ i + "]/td[" + columnNumber+ "]//*";
			
		}
		return By.xpath(".//*[@id='"+tableID+ "_data']/tr[" + i + "]/td["+ targetColumnNumber + "]/*[" + columnContent + "]");
		
	}

	public static void selectDataSetFromDataTable(String uuid) {
		TestFunctions.findAndWaitOnElement(By.xpath(".//*[contains(text(),'" + uuid + "')]/../../td[1]/div[1]")).click();
	}
	
	public static Boolean selectOptionInDataSetDialogue(int option) {
		if (option < 1 || option > 4) {
			return false;
		} else {
				$x(".//*[@id='generalForm']/div[last()]/div[2]/fieldset[1]/legend[1]").click();
				$x(".//*[@id='generalForm']/div[last()]/div[2]/fieldset[1]/div[1]/table[1]/tbody[1]/tr[2]/td[1]/table[1]/tbody[1]/tr["+ option + "]/td[1]/label[1]").click();
				$x(".//*[@id='generalForm']/div[last()]/div[2]/div[2]/span[1]/button[1]/span[1]").click();
			return true;
		}
	}
	
	/**
	 * Extracts password from given mail.
	 * @param message The message the mail contains
	 * @return The password the mail contains
	 * @throws IOException
	 * @throws MessagingException
	 */
	public static String extractPasswordFomMail(MimeMessage message) throws IOException, MessagingException {
		String content = String.valueOf(message.getContent());
		String[] contentSplits2 = content.split("b>Password</b>: ");
		String passw = contentSplits2[1].split("\r")[0];
		return passw;
	}
	
	/**
	 * Extracts user name from given mail.
	 * @param message The message the mail contains
	 * @return The user name the mail contains
	 * @throws IOException
	 * @throws MessagingException
	 */
	public static String extractUserFomMail(MimeMessage message) throws IOException, MessagingException {
		String content = String.valueOf(message.getContent());
		String[] contentSplits2 = content.split("b>Login</b>: ");
		String user = contentSplits2[1].split("<br/>")[0];
		return user;
	}
	
	/**
	 * Extracts link from given mail. 
	 * @param message The message which is contained in mail
	 * @return The link the mail contains
	 * @throws IOException
	 * @throws MessagingException
	 */
	public static String extractLinkFomMail(MimeMessage message) throws IOException, MessagingException {
		String content = String.valueOf(message.getContent());
		String[] contentSplits2 = content.split("href=\"");
		String link = contentSplits2[1].split("\">")[0];
		link = link.replace("&amp;", "&");
		return link;
	}
	
	/**
	 * Creates a Client.
	 * @return A created client
	 */
	public static Client createClient() {
		Client client;
		ClientConfig cc = new DefaultClientConfig();
		cc.getClasses().add( JAXBContextResolver.class );
		cc.getProperties().put( ClientConfig.PROPERTY_FOLLOW_REDIRECTS, true );
		cc.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		client = Client.create( cc );
		return client;
	}
	
	/**
	 * Generates an API key via GET request to REST service API.
	 * @param userName The name of user that shall generate an API key
	 * @param password The password for given user
	 * @param expectedStatus The expected response status code
	 * @return A String containing either the API key or an error message
	 */
	public static String generateAPIKeyGET(Client client, String userName, String password, int expectedStatus) {
		String APIKey = null;
		ClientResponse response = client.resource(TestContext.PRIMARY_SITE_URL + "resource/authenticate/getToken?userName="+ userName + "&password=" + password).get(ClientResponse.class);
		
		int status = response.getStatus();
		
			APIKey = response.getEntity(String.class);
		
		if (status != expectedStatus) {
			org.testng.Assert.fail("User with name " + userName + " should get response code " + expectedStatus + " after requesting API key but gets response code " + status + ".");
		}
		
		return APIKey;
	}
	
	/**
	 * Checks with API key whether wanted dataset occurs in dataset list of given
	 * type in given data stock.
	 * 
	 * @param APIKey     The API key for authentication
	 * @param stock_id   The UUID of data stock to check
	 * @param dataset_id The UUID of datast to check if it exists
	 * @param type       The type of dataset to check
	 * @param expected   The flag indicating whether dataset should occur or not.
	 */
	public static void checkDatasetExisting(String APIKey, String stock_id, String dataset_id, String type, boolean expected,
			String header_name, Client client) {
		boolean datasetVisible = false;
		ClientResponse response;
		
		String url = TestContext.PRIMARY_SITE_URL + "resource/datastocks/" + stock_id + "/" + type;
		
		log.debug("calling URL " + url);
		
		if (APIKey != null) {
			response = client.resource(url).header(header_name, "Bearer " + APIKey).get(ClientResponse.class);
		} else {
			response = client.resource(url).get(ClientResponse.class);
		}

		int status = response.getStatus();

		log.debug("response is HTTP " + status);
		
		if (status == 200) {

			// Constructing the list of datasets out of response
			Object result = response.getEntity(DataSetList.class);

			JAXBElement<DataSetList> e = (JAXBElement<DataSetList>) result;

			DataSetList list = e.getValue();

			List<DataSetVO> datasetList = list.getDataSet();
			
			log.debug("result set has " + datasetList.size() + " entries");

			// Going through stock list and check whether stock UUID equals UUID of
			// restricted stock
			for (DataSetVO datasetVO : datasetList) {

				if (datasetVO.getUuidAsString().equals(dataset_id)) {
					datasetVisible = true;
					continue;
				}

			}

		}

		if (expected != datasetVisible) {
			String shouldNot = "";
			String isNot = " not";
			if (datasetVisible) {
				isNot = "";
				shouldNot = " not";

			}
			String message = "The " + type + " dataset " + dataset_id + " should" + shouldNot + " be visible in data stock "
					+ stock_id + " but seems" + isNot + " to be.";
			log.error(message);
			org.testng.Assert.fail(message);
		}

	}


}
