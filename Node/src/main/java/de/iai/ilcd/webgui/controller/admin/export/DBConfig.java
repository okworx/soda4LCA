package de.iai.ilcd.webgui.controller.admin.export;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

@Configuration
public class DBConfig {

	@Bean
	public static DataSource getDataSource() {
		return new JndiDataSourceLookup().getDataSource("jdbc/soda4LCAdbconnection");
	}

	@Bean
	public static JdbcTemplate getJdbcTemplate() {
		return new JdbcTemplate(getDataSource());
	}

	@Bean
	public static NamedParameterJdbcTemplate getNamedJdbcTemplate() {
		return new NamedParameterJdbcTemplate(getDataSource());
	}
}
