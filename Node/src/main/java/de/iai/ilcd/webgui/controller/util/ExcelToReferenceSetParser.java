package de.iai.ilcd.webgui.controller.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.DataFormatException;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fzk.iai.ilcd.service.model.enums.GlobalReferenceTypeValue;
import de.iai.ilcd.model.common.DataSetVersion;
import de.iai.ilcd.model.common.GlobalReference;
import de.iai.ilcd.model.common.Uuid;
import de.iai.ilcd.model.common.exception.FormatException;
import groovy.lang.MissingPropertyException;

public class ExcelToReferenceSetParser {

	public static Logger logger = LoggerFactory.getLogger(ExcelToReferenceSetParser.class);

	private int typeColumnNumber;

	private int uuidColumnNumber;

	private int versionColumnNumber;

	private Boolean columnsIdentified = false;

	private int invalidRowsCounter = 0;

	/**
	 * Parses a set of references from a suitable Excel file.
	 * 
	 * @param excelFile
	 * @return
	 * @throws IOException
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 * @throws DataFormatException
	 */
	public Set<GlobalReference> parseGlobalReferencesFrom(InputStream excelInputStream)
			throws DataFormatException, EncryptedDocumentException, IOException, InvalidFormatException {
		invalidRowsCounter = 0;
		Workbook wb = WorkbookFactory.create(excelInputStream); // throws EncryptedDocumentException, IOException,
																// InvalidFormatException

		Set<GlobalReference> result = new HashSet<GlobalReference>();

		for (int i = 0; i < wb.getNumberOfSheets(); i++) {// We must iterate through sheets, then rows, then cells.
			Sheet sheet = wb.getSheetAt(i);
			determineRelevantColumns(sheet);
			if (columnsIdentified) {
				for (Row row : sheet) {
					int rowNumber = row.getRowNum();
					if (rowNumber != 0) {
						int displayedRowNumber = rowNumber + 1;
						GlobalReference ref = new GlobalReference();

							try {
								ref = parseGlobalReferenceFrom(row);  	// the actual parsing happens here.
							} catch (MissingPropertyException | DataFormatException | FormatException e) {
								if (logger.isDebugEnabled())
									logger.debug("Row " + displayedRowNumber + ": " + e.getMessage() + "");
							}

						if (isValid(ref)) {
							result.add(ref);
						} else {
							if(!isEmpty(row)) // We care about corrupted entries not weird Excel behaviour.
								invalidRowsCounter += 1;
						}
					}
				}
			}
		}
		if (result.isEmpty())
			throw new DataFormatException(
					"Excel file could not be parsed. If it contains entries, non of the entries match the minimum requirements: Having 'Dataset Type', 'UUID', 'Version' specified.");
		if (invalidRowsCounter > 0)
			logger.warn(invalidRowsCounter + " rows could not be parsed.");
		return result;
	}

	private void determineRelevantColumns(Sheet sheet) {
		Boolean typeColumnNumberUnascertained = true; // We settle for the first matching columns.
		Boolean uuidColumnNumberUnascertained = true;
		Boolean versionColumnNumberUnascertained = true;
		Row row = sheet.getRow(0);
		for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
			Cell cell = row.getCell(i);
			String thisColumnsTitle = "";
			try {
				thisColumnsTitle = getStringValueOf(cell);
			} catch (DataFormatException e) {
				// Handled elsewhere.
			}
			if (typeColumnNumberUnascertained && StringUtils.containsIgnoreCase(thisColumnsTitle, "typ")) {
				typeColumnNumber = cell.getColumnIndex();
				typeColumnNumberUnascertained = false;
			}
			if (uuidColumnNumberUnascertained && StringUtils.containsIgnoreCase(thisColumnsTitle, "uuid")) {
				uuidColumnNumber = cell.getColumnIndex();
				uuidColumnNumberUnascertained = false;
			}
			if (versionColumnNumberUnascertained && StringUtils.containsIgnoreCase(thisColumnsTitle, "version")) {
				versionColumnNumber = cell.getColumnIndex();
				versionColumnNumberUnascertained = false;
			}
		}
		if (!typeColumnNumberUnascertained && !uuidColumnNumberUnascertained && !versionColumnNumberUnascertained)
			columnsIdentified = true;
	}

	/**
	 * Parsing <code>GlobalReference<code> from a row which is expected to contain
	 * UUIDs , GlobalReferenceTypeValues and DataSetVersions. 
	 * 
	 * @param row
	 * @return
	 * @throws DataFormatException
	 * @throws MissingPropertyException
	 * @throws FormatException
	 */
	private GlobalReference parseGlobalReferenceFrom(Row row)
			throws DataFormatException, MissingPropertyException, FormatException {
		if(isEmpty(row))
			return null;
		GlobalReference ref = new GlobalReference();
		String uuidString = new String();
		String versionString = new String();
		String typeString = new String();
		int rowNumber = row.getRowNum() + 1;
		uuidString = getStringValueOf(row.getCell(uuidColumnNumber));
		versionString = getStringValueOf(row.getCell(versionColumnNumber));
		typeString = getStringValueOf(row.getCell(typeColumnNumber));
		
		//Checking for empty strings, throwing exceptions..
		if (uuidString.isEmpty())
			throw new MissingPropertyException("UUID not found.");
		ref.setUuid(new Uuid(uuidString));
		if (typeString.isEmpty())
			throw new MissingPropertyException("data set type not found.");
		if(versionString.isEmpty())
			throw new MissingPropertyException("version not found.");

		//Trying to parse GlobalReferenceTypeValue
		try {
			ref.setType(GlobalReferenceTypeValue.fromValue(typeString));
			if(ref.getType() == null)
				throw new IllegalArgumentException();
		} catch (IllegalArgumentException e) {
			if(logger.isDebugEnabled())
				logger.debug("Row " + rowNumber + ": Dataset type could not be parsed.");
		}
		try {
			ref.setVersion(DataSetVersion.parse(versionString));
		} catch (FormatException e) {
			// Don't set the version number.
			if(logger.isDebugEnabled())
				logger.debug("Row " + rowNumber + ": Version could not be parsed.");
		}
		return ref;
	}

	/**
	 * Gets the String value of a cell. If the CellType is invalid it returns an
	 * empty String and throws a FormatException.
	 * 
	 * In case of POI-Upgrade: You might need <code> if(cell.getCellType() ==
	 * CellType.String) <code> instead.
	 * 
	 * @param cell
	 * @return
	 * @throws FormatException
	 */
	private String getStringValueOf(Cell cell) throws DataFormatException {
		StringBuilder value = new StringBuilder("");
		if ( cell != null) {
				if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
					value.append(cell.getStringCellValue());
				} else {
					throw new DataFormatException("Invalid CellType: CellType should be CellType.STRING.");
				}
		}
		return value.toString();
	}

	/**
	 * We consider a <code>GlobalReference<code> as valid, when it contains some
	 * String as UUID.
	 * 
	 * @param ref
	 * @return
	 */
	private boolean isValid(GlobalReference ref) {
		if (ref == null || ref.getUuid() == null || ref.getUuid().getUuid().isEmpty() || ref.getType() == null)
			return false;
		return true;
	}

	public int getNumberOfFailures() {
		return invalidRowsCounter;
	}
	
	private Boolean isEmpty(Row row) {
		if(row == null || row.getLastCellNum()<=0)
			return true;
		return false;
	}
}
