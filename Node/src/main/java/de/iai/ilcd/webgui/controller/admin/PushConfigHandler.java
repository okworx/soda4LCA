package de.iai.ilcd.webgui.controller.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.codehaus.plexus.util.StringUtils;
import org.quartz.SchedulerException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;

import de.fzk.iai.ilcd.service.client.ILCDServiceClientException;
import de.iai.ilcd.model.common.PushConfig;
import de.iai.ilcd.model.common.PushTarget;
import de.iai.ilcd.model.dao.MergeException;
import de.iai.ilcd.model.dao.PersistException;
import de.iai.ilcd.model.dao.ProcessDao;
import de.iai.ilcd.model.dao.PushConfigDao;
import de.iai.ilcd.model.dao.PushTargetDao;
import de.iai.ilcd.model.datastock.AbstractDataStock;
import de.iai.ilcd.rest.util.NotAuthorizedException;
import de.iai.ilcd.service.PushService;
import de.iai.ilcd.service.exception.JobNotScheduledException;
import de.iai.ilcd.service.exception.UserNotExistingException;
import de.iai.ilcd.util.DependenciesOptions;
import de.iai.ilcd.webgui.controller.url.URLGeneratorBean;

/**
 * Handler for push configuration entries.
 * 
 * @author sarai
 *
 */
@ManagedBean
@ViewScoped
public class PushConfigHandler extends AbstractAdminEntryHandler<PushConfig> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1991027201516698087L;

	/*
	 * The logger for logging data
	 */
	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());

	/*
	 * Service for scheduling push jobs
	 */
	private final PushService pushService;

	/*
	 * The list of decidable push target entries
	 */
	private List<PushTarget> pushTargets = new ArrayList<PushTarget>();

	/*
	 * The dao for finding push configs
	 */
	private PushConfigDao pushConfigDao = new PushConfigDao();

	/*
	 * The password for connection to target node
	 */
	private String password;

	/*
	 * The decidable dependency options
	 */
	private DependenciesOptions depOptions = new DependenciesOptions();

	/*
	 * The handler for managing source stocks
	 */
	@ManagedProperty(value = "#{stockListHandler}")
	private StockListHandler stockListHandler = new StockListHandler();

	/*
	 * Flag for checking whether current view is editable
	 */
	private boolean edit = false;
	
	/*
	 * Flag for checking whether current config is already pushed 
	 */
	private boolean pushed = false;

	public PushConfigHandler() {
		super();
		WebApplicationContext ctx = FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance());
		this.pushService = ctx.getBean(PushService.class);
	}

	/**
	 * Checks if current view of push config entry is editable
	 * 
	 * @return
	 */
	public boolean isEdit() {
		return edit;
	}

	/**
	 * sets editable flag of current view to given flag
	 * 
	 * @param edit
	 */
	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	/**
	 * Gets the dependencies option.
	 * 
	 * @return dependencies option for push configuration
	 */
	public DependenciesOptions getDependenciesOptions() {
		return this.depOptions;
	}

	/**
	 * Gets list of all selectable push targets.
	 * 
	 * @return list of all saved push targets
	 */
	public List<PushTarget> getPushTargets() {
		return this.pushTargets;
	}

	/**
	 * Sets the list of all push targets.
	 * 
	 * @param pushTargets
	 *            the list of all push targets
	 */
	public void setPushTargets(List<PushTarget> pushTargets) {
		this.pushTargets = pushTargets;
	}

	/**
	 * Gets the current stock handler for managing available source stocks.
	 * 
	 * @return A StockHandler for managing available source stocks
	 */
	public StockListHandler getStockListHandler() {
		return stockListHandler;
	}

	/**
	 * Sets the StockHandler for managing source stocks.
	 * 
	 * @param stockListHandler
	 *            The stock handler for managing source stocks
	 */
	public void setStockListHandler(StockListHandler stockListHandler) {
		this.stockListHandler = stockListHandler;
	}

	/**
	 * Gets entered password of target node.
	 * 
	 * @return The entered password of target node
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * Sets the entered password of target node.
	 * 
	 * @param password
	 *            The entered password for target node
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the current push configuration entry.
	 * 
	 * @return An entry of the current push configuration
	 */
	public PushConfig getPushConfig() {
		return this.getEntry();
	}

	/**
	 * Sets the current push configuration entry.
	 * 
	 * @param entry
	 *            The entry of current push configuration
	 */
	public void setPushConfig(PushConfig entry) {
		this.setEntry(entry);
	}

	/**
	 * Creates a new push configuration entry if all entries of form were filled
	 * in correctly.
	 * 
	 * @return true if name of push configuration did not exist yet and if entry
	 *         is successfully stored in data table.
	 */
	public boolean createPushConfig() {
		PushConfig pushConfig = this.getEntry();
		if (pushConfig.getDependenciesMode() == null) {
			this.addI18NFacesMessage("facesMsg.pushConfig.noDependency", FacesMessage.SEVERITY_ERROR);
			return false;
		}
		if (pushConfig.getName() == null) {
			this.addI18NFacesMessage("facesMsg.pushConfig.noName", FacesMessage.SEVERITY_ERROR);
			return false;
		}
		if (pushConfig.getSource() == null) {
			this.addI18NFacesMessage("facesMsg.pushConfig.noSource", FacesMessage.SEVERITY_ERROR);
			return false;
		}
		if (pushConfig.getTarget() == null) {
			this.addI18NFacesMessage("facesMsg.pushConfig.noTarget", FacesMessage.SEVERITY_ERROR);
			return false;
		}
		PushConfig existingPushConfig = this.pushConfigDao.getPushConfig(pushConfig.getName());
		if (existingPushConfig != null) {
			this.addI18NFacesMessage("facesMsg.pushConfig.alreadyExists", FacesMessage.SEVERITY_ERROR);
			return false;
		}
		try {
			this.pushConfigDao.persist(pushConfig);
			this.addI18NFacesMessage("facesMsg.pushConfig.createSuccess", FacesMessage.SEVERITY_INFO);
			return true;
		} catch (PersistException pe) {
			log.warn("Could not persist PushConfig entry!");
			if (log.isDebugEnabled()) {
				pe.printStackTrace();
			}
			this.addI18NFacesMessage("facesMsg.pushConfig.createError", FacesMessage.SEVERITY_ERROR);
			return false;
		}
	}

	/**
	 * Changes the current push configuration entry.
	 * 
	 * @return true if push configuration entry does already exist and new data
	 *         are successfully merged into existing entry
	 */
	public boolean changePushConfig() {
		final PushConfig pushConfig = this.getEntry();
		final Long pushConfigId = pushConfig.getId();
		if (StringUtils.isBlank(pushConfig.getName())) {
			this.addI18NFacesMessage("facesMsg.pushConfig.noName", FacesMessage.SEVERITY_ERROR);
			return false;
		}
		if (pushConfig.getDependenciesMode() == null) {
			this.addI18NFacesMessage("facesMsg.pushConfig.noDependency", FacesMessage.SEVERITY_ERROR);
			return false;
		}
		if (pushConfig.getTarget() == null) {
			this.addI18NFacesMessage("facesMsg.pushConfig.noTarget", FacesMessage.SEVERITY_ERROR);
			return false;
		}
		if (pushConfig.getSource() == null) {
			this.addI18NFacesMessage("facesMsg.pushConfig.noSource", FacesMessage.SEVERITY_ERROR);
			return false;
		}
		PushConfig existingPushConfig = this.pushConfigDao.getPushConfig(pushConfig.getName());
		if (existingPushConfig != null && !existingPushConfig.getId().equals(pushConfigId)) {
			this.addI18NFacesMessage("facesMsg.pushConfig.alreadyExists", FacesMessage.SEVERITY_ERROR);
			return false;
		}

		try {
			this.setEntry(this.pushConfigDao.merge(pushConfig));
			this.addI18NFacesMessage("facesMsg.pushConfig.changeSuccess", FacesMessage.SEVERITY_INFO);
			return true;
		} catch (Exception e) {
			log.warn("Could not change PushConfigEntry!");
			if (log.isDebugEnabled()) {
				e.printStackTrace();
			}
			this.addI18NFacesMessage("facesMsg.saveDataError", FacesMessage.SEVERITY_ERROR);
			return false;
		}
	}

	/**
	 * Pushes source stock with dependency mode to target stock of target node.
	 */
	public final void pushDataStock() {
		try {
			log.debug("pushing data stock on handler");
			pushService.pushDataStock(this.getEntry(), password, this.getCurrentUser());
		} catch (SchedulerException se) {
			log.warn("Could not schedule push job!");
			if (log.isDebugEnabled()) {
				se.printStackTrace();
			}
			this.addI18NFacesMessage("facesMsg.push.networkError", FacesMessage.SEVERITY_ERROR);
		} catch (PersistException e) {
			this.addI18NFacesMessage("facesMsg.jobScheduling.persistError", FacesMessage.SEVERITY_ERROR);
			log.warn("Could not persist JobMetaData entry of PushConfig job!");
			if (log.isDebugEnabled())
				e.printStackTrace();
		} catch (MergeException e) {
			this.addI18NFacesMessage("facesMsg.jobScheduling.stateNotUpdated", FacesMessage.SEVERITY_ERROR);
			log.warn("Could not update job state of push job!");
			if (log.isDebugEnabled())
				e.printStackTrace();
		} catch (UserNotExistingException e) {
			this.addI18NFacesMessage("facesMsg.jobScheduling.userNotExisting", FacesMessage.SEVERITY_ERROR);
			log.warn("Current user does not exist!");
			if (log.isDebugEnabled())
				e.printStackTrace();
		} catch (JobNotScheduledException e) {
			this.addI18NFacesMessage("facesMsg.jobScheduling.jobNotScheduled", FacesMessage.SEVERITY_ERROR);
			log.warn("Push job could not be scheduled!");
			if (log.isDebugEnabled())
				e.printStackTrace();
		} catch (ILCDServiceClientException e) {
			this.addI18NFacesMessage("facesMsg.push.networkError", FacesMessage.SEVERITY_ERROR);
			log.warn("Could not initialize network client!");
			if (log.isDebugEnabled())
				e.printStackTrace();
		} catch (NotAuthorizedException e) {
			log.warn("User is not authorized!");
			if (log.isDebugEnabled())
				e.printStackTrace();
		}
		
		pushed = true;
	}

	/**
	 * Gets the number of process data sets contained in source data stock.
	 * 
	 * @return The number of process data set contained in source data stock
	 */
	public Long getDataSetCount() {
		Long result = null;
		ProcessDao processDao = new ProcessDao();
		try {
			result = processDao.getCount(this.getEntry().getSource());
		} catch (NullPointerException e) {
			log.warn("Source stock is null!");
			if (log.isDebugEnabled()) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	public Long getImportedDataSetsAfterPush() {
		Long result = null; 
		ProcessDao dsDao = new ProcessDao();
		AbstractDataStock[] stocks = {this.getEntry().getSource()};
		Date lastPushDate = this.getEntry().getLastPushDate();
		if (lastPushDate != null)
			result = dsDao.getCountImportedAfter(stocks, null, lastPushDate.getTime());
		return result;
	}

	/**
	 * Gets the number of process data sets contained in source data set as
	 * String.
	 * 
	 * @return The number of process data set contained in source data stock as
	 *         String
	 */
	public String getDataSetCountAsString() {
		return getDataSetCount().toString();
	}
	
	public String getImportedDataSetsAfterPushAsString() {
		Long count = getImportedDataSetsAfterPush();
		
		String result = count!=null ? count.toString() : "";
		return (result );
	}
	
	public boolean isFavourite() {
		return this.getEntry().isFavourite();
	}
	
	public void setFavourite(boolean favourite) {
		this.getEntry().setFavourite(favourite);
	}
	
	public boolean isPushed() {
		return pushed;
	}
	
	public void setPushed(boolean pushed) {
		this.pushed = pushed;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void postEntrySet() {
		PushTargetDao pTDao = new PushTargetDao();
		pushTargets = pTDao.getAll();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected PushConfig createEmptyEntryInstance() {
		return new PushConfig();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected PushConfig loadEntryInstance(long id) throws Exception {
		return this.pushConfigDao.getById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void postConstruct() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean createEntry() {
		return createPushConfig();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean changeAttachedEntry() {
		return changePushConfig();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEditEntryUrl(URLGeneratorBean url, Long id) {
		return url.getPushConfig().getEdit(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCloseEntryUrl(URLGeneratorBean url) {
		return url.getPushConfig().getShowList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getNewEntryUrl(URLGeneratorBean url) {
		return url.getPushConfig().getNew();
	}

}
