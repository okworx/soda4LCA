package de.iai.ilcd.webgui.controller.util;

import java.io.IOException;
import java.io.Writer;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import de.fzk.iai.ilcd.service.model.common.IClass;
import de.fzk.iai.ilcd.service.model.process.IComplianceSystem;
import de.iai.ilcd.configuration.ConfigurationService;
import de.iai.ilcd.model.datastock.ExportType;
import de.iai.ilcd.model.flow.MaterialProperty;
import de.iai.ilcd.model.flow.ProductFlow;
import de.iai.ilcd.model.process.Process;

public class CSVFormatter {

	private static DecimalSeparator decimalSeparator = DecimalSeparator.DOT;
	private static String entrySeparator = ";";

	protected enum indicatorsHeaderEnum {
		GWP("77e416eb-a363-4258-a04e-171d843a6460", IndicatorsValueType.LCIAResult),
		ODP("06dcd26f-025f-401a-a7c1-5e457eb54637", IndicatorsValueType.LCIAResult),
		POCP("1e84a202-dae6-42aa-9e9d-71ea48b8be00", IndicatorsValueType.LCIAResult),
		AP("b4274add-93b7-4905-a5e4-2e878c4e4216", IndicatorsValueType.LCIAResult),
		EP("f58827d0-b407-4ec6-be75-8b69efb98a0f", IndicatorsValueType.LCIAResult),
		ADPE("f7c73bb9-ab1a-4249-9c6d-379a0de6f67e", IndicatorsValueType.LCIAResult),
		ADPF("804ebcdf-309d-4098-8ed8-fdaf2f389981", IndicatorsValueType.LCIAResult),

		PERE("20f32be5-0398-4288-9b6d-accddd195317", IndicatorsValueType.Exchange),
		PERM("fb3ec0de-548d-4508-aea5-00b73bf6f702", IndicatorsValueType.Exchange),
		PERT("53f97275-fa8a-4cdd-9024-65936002acd0", IndicatorsValueType.Exchange),
		PENRE("ac857178-2b45-46ec-892a-a9a4332f0372", IndicatorsValueType.Exchange),
		PENRM("1421caa0-679d-4bf4-b282-0eb850ccae27", IndicatorsValueType.Exchange),
		PENRT("06159210-646b-4c8d-8583-da9b3b95a6c1", IndicatorsValueType.Exchange),
		SM("c6a1f35f-2d09-4f54-8dfb-97e502e1ce92", IndicatorsValueType.Exchange),
		RSF("64333088-a55f-4aa2-9a31-c10b07816787", IndicatorsValueType.Exchange),
		NRSF("89def144-d39a-4287-b86f-efde453ddcb2", IndicatorsValueType.Exchange),
		FW("3cf952c8-f3a4-461d-8c96-96456ca62246", IndicatorsValueType.Exchange),
		HWD("430f9e0f-59b2-46a0-8e0d-55e0e84948fc", IndicatorsValueType.Exchange),
		NHWD("b29ef66b-e286-4afa-949f-62f1a7b4d7fa", IndicatorsValueType.Exchange),
		RWD("3449546e-52ad-4b39-b809-9fb77cea8ff6", IndicatorsValueType.Exchange),
		CRU("a2b32f97-3fc7-4af2-b209-525bc6426f33", IndicatorsValueType.Exchange),
		MFR("d7fe48a5-4103-49c8-9aae-b0b5dfdbd6ae", IndicatorsValueType.Exchange),
		MER("59a9181c-3aaf-46ee-8b13-2b3723b6e447", IndicatorsValueType.Exchange),
		EEE("4da0c987-2b76-40d6-9e9e-82a017aaaf29", IndicatorsValueType.Exchange),
		EET("98daf38a-7a79-46d3-9a37-2b7bd0955810", IndicatorsValueType.Exchange);

		String UUID;
		IndicatorsValueType vt;

		indicatorsHeaderEnum(String UUID, IndicatorsValueType vt) {
			this.UUID = UUID;
			this.vt = vt;
		}

		String getResultValue(Process process, String module) {
			switch (vt) {
			case LCIAResult:
				return getLciaResultValue(process, UUID, module);
			case Exchange:
				return getExchangeValue(process, UUID, module);
			default:
				return "MISSING IndicatorsValueType";
			}

		}

		private String getExchangeValue(Process process, String indicatorUUID, String module) {
			try {
				return format(
						process.getExchangesByIndicator(indicatorUUID).get(0).getAmountByModule(module).getValue());
			} catch (Exception e) {
				return "";
			}
		}

		private String getLciaResultValue(Process process, String indicatorUUID, String module) {
			try {
				return format(
						process.getLciaResultsByIndicator(indicatorUUID).get(0).getAmountByModule(module).getValue());
			} catch (Exception e) {
				return "";
			}
		}

		@Override
		public String toString() {
			return this.name();
		}

		enum IndicatorsValueType {
			LCIAResult, Exchange;
		};

	}

	protected enum HeaderEnum {
		UUID("UUID", "UUID"),
		Version("Version", "Version"),
		Name("Name", "Name"),
		Category("Category", "Kategorie"),
		Compliance("Compliance", "Konformität"),
		Type("Type", "Typ"),
		Refquantity("Ref. quantity", "Bezugsgroesse"),
		Refunit("Ref. unit", "Bezugseinheit"),
		ReferenceflowUUID("Reference flow UUID", "Referenzfluss-UUID"),
		Referenceflowname("Reference flow name", "Referenzfluss-Name"),
		Bulkdensity_kgm3("Bulk Density (kg/m3)", "Schuettdichte (kg/m3)"),
		Grammage_kgm2("Grammage (kg/m2)", "Flaechengewicht (kg/m2)"),
		Grossdensity_kgm3("Gross Density (kg/m3)", "Rohdichte (kg/m3)"),
		Layerthickness_m("Layer Thickness (m)", "Schichtdicke (m)"),
		Productiveness_m2("Productiveness (m2)", "Ergiebigkeit (m2)"),
		Lineardensity_kgm("Linear Density (kg/m)", "Laengengewicht (kg/m)"),
		Conversionfactorto1kg("Conversion factor to 1kg", "Umrechungsfaktor auf 1kg"),
		Module("Module", "Modul");

		String title_en, title_de;

		private HeaderEnum(String en, String de) {
			this.title_en = en;
			this.title_de = de;
		}

	}

	public CSVFormatter(DecimalSeparator decimalSeparator) {
		CSVFormatter.decimalSeparator = decimalSeparator;
	}

	public void writeHeader(Writer w) throws IOException {

		Function<HeaderEnum, String> langMapper;
		if ("de".equals(ConfigurationService.INSTANCE.getDefaultLanguage()))
			langMapper = h -> h.title_de;
		else
			langMapper = h -> h.title_en;

		for (HeaderEnum h : HeaderEnum.values())
			w.write(langMapper.apply(h) + entrySeparator);

		for (indicatorsHeaderEnum h : indicatorsHeaderEnum.values())
			w.write(h.toString() + entrySeparator);

		w.write("\n");
	}

	public void formatCSV(List<Process> processes, Map<String, ProductFlow> flowProperties, Writer w)
			throws IOException {

		for (Process process : processes) {

			StringBuilder sb = new StringBuilder();
			sb.append(process.getUuidAsString());
			sb.append(entrySeparator);
			sb.append(process.getDataSetVersion());
			sb.append(entrySeparator);
			try {
				sb.append(process.getBaseName().getDefaultValue().replaceAll(";", ","));
			} catch (NullPointerException e) {
			}
			sb.append(entrySeparator);
			try {
				// [a, b, c] -> "a" / "b" / "c"				
				sb.append(process.getClassification().getIClassList().stream().map(IClass::getName)
						.collect(Collectors.joining("\' / \'", "\'", "\'")));	
			} catch (NullPointerException e) {
			}
			sb.append(entrySeparator);
			try {
				sb.append(process.getComplianceSystems().stream().map(IComplianceSystem::getName)
						.collect(Collectors.joining("\' / \'", "\'", "\'")));

			} catch (NullPointerException e) {
			}
			sb.append(entrySeparator);
			
			try {
				sb.append(process.getSubType().getValue());
			} catch (NullPointerException e) {
			}
			sb.append(entrySeparator);
			try {
				sb.append(format((double) process.getReferenceExchanges().get(0).getMeanAmount()));
			} catch (NullPointerException e) {
			}
			sb.append(entrySeparator);
			try {
				sb.append(process.getReferenceExchanges().get(0).getReferenceUnit());
			} catch (NullPointerException e) {
			}
			sb.append(entrySeparator);

			ProductFlow productFlow = flowProperties.get(process.getUuid().getUuid());
			sb.append(productFlow.getUuid().getUuid());
			sb.append(entrySeparator);
			try {
				sb.append(productFlow.getName().getDefaultValue().replace(";", ","));
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			sb.append(entrySeparator);
			Map<String, Double> matPropMap = new HashMap<String, Double>();
			for (MaterialProperty matProp : productFlow.getMaterialProperties()) {
				matPropMap.put(matProp.getDefinition().getName(), matProp.getValue());
			}
			if (matPropMap.containsKey("bulk density")) {
				sb.append(format(matPropMap.get("bulk density")));
			}
			sb.append(entrySeparator);
			if (matPropMap.containsKey("grammage")) {
				sb.append(format(matPropMap.get("grammage")));
			}
			sb.append(entrySeparator);
			if (matPropMap.containsKey("gross density")) {
				sb.append(format(matPropMap.get("gross density")));
			}
			sb.append(entrySeparator);
			if (matPropMap.containsKey("layer thickness")) {
				sb.append(format(matPropMap.get("layer thickness")));
			}
			sb.append(entrySeparator);
			if (matPropMap.containsKey("productiveness")) {
				sb.append(format(matPropMap.get("productiveness")));
			}
			sb.append(entrySeparator);
			if (matPropMap.containsKey("linear density")) {
				sb.append(format(matPropMap.get("linear density")));
			}
			sb.append(entrySeparator);
			if (matPropMap.containsKey("conversion factor to 1 kg")) {
				sb.append(format(matPropMap.get("conversion factor to 1 kg")));
			}

			sb.append(entrySeparator);

			String metaData = sb.toString();

			for (String module : process.getDeclaredModules()) {
				w.write(metaData);

				w.write(module);
				w.write(entrySeparator);

				for (indicatorsHeaderEnum h : indicatorsHeaderEnum.values())
					w.write(h.getResultValue(process, module) + entrySeparator);

				w.write("\n");
			}
		}
	}

	private static String format(Double d) {
		DecimalFormatSymbols decimalSymbols = DecimalFormatSymbols.getInstance();
		decimalSymbols.setDecimalSeparator(decimalSeparator.separator);
		return new DecimalFormat("#.####################", decimalSymbols).format(d);
	}

	public enum DecimalSeparator {

		DOT("dot", '.'), COMMA("comma", ',');

		private String value;
		public char separator;

		DecimalSeparator(String value, char separator) {
			this.value = value;
			this.separator = separator;
		}

		public static ExportType fromValue(String value) {
			for (ExportType enumValue : ExportType.values()) {
				if (enumValue.getValue().equals(value))
					return enumValue;
			}
			return null;
		}

		public String getValue() {
			return value;
		}
	}
}
