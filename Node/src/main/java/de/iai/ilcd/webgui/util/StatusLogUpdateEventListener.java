package de.iai.ilcd.webgui.util;

import java.io.PrintWriter;

import com.okworx.ilcd.validation.util.IUpdateEventListener;

public class StatusLogUpdateEventListener implements IUpdateEventListener {

	private PrintWriter writer;

	@Override
	public void updateProgress( double percentFinished ) {
		writer.print( "." );
		writer.flush();

	}

	@Override
	public void updateStatus( String statusMessage ) {
		writer.println( statusMessage );
		writer.flush();

	}

	public PrintWriter getWriter() {
		return writer;
	}

	public void setWriter( PrintWriter writer ) {
		this.writer = writer;
	}

}
