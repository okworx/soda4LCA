package de.iai.ilcd.security;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;

import de.iai.ilcd.model.dao.UserDao;
import de.iai.ilcd.model.security.User;

/**
 * The info class for bearer tokens.
 * @author sarai
 *
 */
public class BearerInfo implements AuthenticationInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7124358331031636214L;

	private BearerToken token;
	
	private UserDao dao = new UserDao();
	
	public BearerInfo(BearerToken token) {
		this.token = token;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrincipalCollection getPrincipals() {
		User user = getUser();
		if (user != null) {
			return new SimplePrincipalCollection(user.getUserName(), IlcdSecurityRealm.REALM_NAME);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getCredentials() {
		User user = getUser();
		if (user != null) {
			return user.getApiKey();
		} 
		return null;	
	}
	
	/**
	 * Gets user from token
	 * @return a user resembled in token
	 */
	private User getUser() {
		if (token != null && token.getPrincipal() instanceof String) {
			String userName = (String) token.getPrincipal();
			return  dao.getUser(userName);
		}
		return null;
	}
		
}
