package de.iai.ilcd.security;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

/**
 * The realm for managing bearer token-based requests
 * @author sarai
 *
 */
public class BearerRealm extends AuthorizingRealm {
	
	public BearerRealm() {
		setAuthenticationTokenClass(BearerToken.class);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
    public boolean supports(AuthenticationToken token) {
        return token != null && token instanceof BearerToken;
    }
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		BearerToken bearerToken = (BearerToken) token;
		if (bearerToken != null) {
			AuthenticationInfo authInfo = new BearerInfo(bearerToken);
			if (authInfo != null) {
				return authInfo;
			}
		}
		throw new AuthenticationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		if ( principals != null ) {
			return new IlcdAuthorizationInfo( (String) principals.getPrimaryPrincipal() );
		}
		else {
			return new IlcdAuthorizationInfo();
		}
	}

}
