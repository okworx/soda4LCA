package de.iai.ilcd.util;

import java.util.Set;

import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.dao.DataSetDao;
import de.iai.ilcd.model.dao.DependenciesMode;

public class DependenciesUtil {
	public Set<DataSet> getDependencies(DataSet dataset, DependenciesMode dependencyOptions) {
		DataSetDao<?, ?, ?> dao = DataSetDao.getDao(dataset);
		Set<DataSet> deps = dao.getDependencies(dataset, dependencyOptions);
		return deps;
	}

}
