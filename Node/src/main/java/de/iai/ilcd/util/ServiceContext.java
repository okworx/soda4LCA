package de.iai.ilcd.util;

import java.net.URI;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.iai.ilcd.configuration.ConfigurationService;

/**
 * 
 * @author clemens.duepmeier
 */
public class ServiceContext {

	private final static Logger LOGGER = LoggerFactory.getLogger( ServiceContext.class );

	public ServiceContext() {
	}

	public URI getBaseUri() {
		try {
			return new URI( ConfigurationService.INSTANCE.getNodeInfo().getBaseURL() );
		}
		catch ( URISyntaxException e ) {
			ServiceContext.LOGGER.debug( "Illegal URI syntax in node information: " + ConfigurationService.INSTANCE.getNodeInfo().getBaseURL() );
			return null;
		}
	}

	public String getNodeId() {
		return ConfigurationService.INSTANCE.getNodeId();
	}

	public String getNodeName() {
		return ConfigurationService.INSTANCE.getNodeName();
	}
}
