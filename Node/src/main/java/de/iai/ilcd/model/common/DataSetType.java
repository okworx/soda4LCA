package de.iai.ilcd.model.common;

import de.fzk.iai.ilcd.api.app.contact.ContactDataSet;
import de.fzk.iai.ilcd.api.app.flow.FlowDataSet;
import de.fzk.iai.ilcd.api.app.lciamethod.LCIAMethodDataSet;
import de.fzk.iai.ilcd.api.app.lifecyclemodel.LifeCycleModelDataSet;
import de.fzk.iai.ilcd.api.app.process.ProcessDataSet;
import de.fzk.iai.ilcd.api.app.source.SourceDataSet;
import de.fzk.iai.ilcd.api.app.unitgroup.UnitGroupDataSet;
import de.fzk.iai.ilcd.api.dataset.ILCDTypes;

/**
 * 
 * @author clemens.duepmeier
 */
public enum DataSetType {
	PROCESS("process data set"),
	LCIAMETHOD("lcia method data set"),
	FLOW("flow data set"),
	FLOWPROPERTY("flow property data set"),
	UNITGROUP("unit group data set"),
	SOURCE("source data set"),
	CONTACT("contact data set"),
	LIFECYCLEMODEL("lifecyclemodel data set");

	private String value;

	DataSetType( String value ) {
		this.value = value;
	}

	public static DataSetType fromValue( String value ) {
		for ( DataSetType enumValue : DataSetType.values() ) {
			if ( enumValue.getValue().equals( value ) ) {
				return enumValue;
			}
		}
		return null;
	}

	public String getValue() {
		return this.value;
	}
	
	//TODO: wrap in separate class  
	public ILCDTypes getILCDType() {
		switch (this) {
		case PROCESS:
			return ILCDTypes.PROCESS;
		case FLOW:
			return ILCDTypes.FLOW;
		case CONTACT:
			return ILCDTypes.CONTACT;
		case SOURCE:
			return ILCDTypes.SOURCE;
		case LCIAMETHOD:
			return ILCDTypes.LCIAMETHOD;
		case FLOWPROPERTY:
			return ILCDTypes.FLOWPROPERTY;
		case UNITGROUP:
			return ILCDTypes.UNITGROUP;
		case LIFECYCLEMODEL:
			return ILCDTypes.LIFECYCLEMODEL;
		default:
			return null;
		}
	}
	
	//TODO: wrap in separate class  
		public Class<?> getILCDClass() {
			switch (this) {
			case PROCESS:
				return ProcessDataSet.class;
			case FLOW:
				return FlowDataSet.class;
			case CONTACT:
				return ContactDataSet.class;
			case SOURCE:
				return SourceDataSet.class;
			case LCIAMETHOD:
				return LCIAMethodDataSet.class;
			case FLOWPROPERTY:
				return FlowDataSet.class;
			case UNITGROUP:
				return UnitGroupDataSet.class;
			case LIFECYCLEMODEL:
				return LifeCycleModelDataSet.class;
			default:
				return null;
			}
		}
}
