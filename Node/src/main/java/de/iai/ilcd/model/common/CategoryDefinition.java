package de.iai.ilcd.model.common;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table( name = "categorydefinition" )
public class CategoryDefinition {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Long id;

	@OneToOne( cascade = CascadeType.ALL, fetch = FetchType.LAZY )
	@JoinColumn( name = "XMLFILE_ID" )
	XmlFile xmlFile;

	@Temporal( TemporalType.TIMESTAMP )
	private Date importDate = null;

	@Basic
	private boolean mostRecentVersion;

	public XmlFile getXmlFile() {
		return xmlFile;
	}

	public void setXmlFile(XmlFile xmlFile) {
		this.xmlFile = xmlFile;
	}

	public Date getImportDate() {
		return importDate;
	}

	public void setImportDate(Date importDate) {
		this.importDate = importDate;
	}

	public boolean isMostRecentVersion() {
		return mostRecentVersion;
	}

	public void setMostRecentVersion(boolean mostRecentVersion) {
		this.mostRecentVersion = mostRecentVersion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
