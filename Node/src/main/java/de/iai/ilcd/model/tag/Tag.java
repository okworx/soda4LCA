package de.iai.ilcd.model.tag;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import de.iai.ilcd.model.datastock.AbstractDataStock;
import de.iai.ilcd.model.process.Process;


@Entity
@Table( name = "tag" )
public class Tag implements Serializable {
	
	/**
	 * Serialization ID
	 */
	private static final long serialVersionUID = -8653425645726217710L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Long id;
	
	@Column( name="name" )
	private String name;
	
	@Column( name="description" )
	private String description;
	
	@ManyToMany( mappedBy="tags" )
	protected Set<Process> processes = new HashSet<Process>();
	
	@ManyToMany( mappedBy="tags" )
	protected Set<AbstractDataStock> ds = new HashSet<AbstractDataStock>();

	/**
	 * @return the ID of Tag
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the ID of Tag to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the processes
	 */
	public Set<Process> getProcesses() {
		return processes;
	}

	/**
	 * @param processes the processes to set
	 */
	public void setProcesses(Set<Process> processes) {
		this.processes = processes;
	}

	/**
	 * @return the ds
	 */
	public Set<AbstractDataStock> getDs() {
		return ds;
	}

	/**
	 * @param ds the ds to set
	 */
	public void setDs(Set<AbstractDataStock> ds) {
		this.ds = ds;
	}

	/**
	 * @return the tag
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param tag the tag to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean containedInList(List<Tag> tagList) {
	    for (Tag t : tagList) {
            if (name.equals(t.getName()))
                return true;
        }
        return false;
	}
	
	

}
