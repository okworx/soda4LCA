package de.iai.ilcd.model.dao;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Facility for caching the results of distributed queries
 */
public enum ForeignNodesDataCache {

	INSTANCE;

	public static final String REFERENCEYEARS = "referenceyears";
	
	public static final String VALIDUNTILYEARS = "validuntilyears";
	
	public static final String LOCATIONS = "locations";

	public static final String LANGUAGES = "languages";

	public static final String COMPLIANCESYSTEMS = "complianceSystems";

	public static final String DATABASES = "databases";

	/**
	 * The time in milliseconds that a cache entry will be valid from the time
	 * it is stored
	 */
	public static final int ENTRY_TTL = 86400000; // 24 hours

	private ConcurrentHashMap<String, CacheEntry> cache = new ConcurrentHashMap<String, CacheEntry>();

	private static final Logger logger = LoggerFactory.getLogger(ForeignNodesDataCache.class);

	public void put(String key, List<? extends Object> data) {
		if (logger.isTraceEnabled()) {
			logger.trace("cache size is " + this.cache.size());
			logger.trace("putting item in cache " + key);
		}

		this.cache.put(key, new CacheEntry(data));

		if (logger.isTraceEnabled())
			logger.trace("new cache size is " + this.cache.size());
	}

	public List<? extends Object> get(String key) {
		if (logger.isTraceEnabled())
			logger.trace("cache size is " + this.cache.size());

		cleanUp();

		CacheEntry item = this.cache.get(key);
		if (item == null) {
			logger.trace("get: cache miss");
			return null;
		} else {
			logger.debug("get: cache hit");
			if (item.isExpired()) {
				if (logger.isTraceEnabled())
					logger.trace("but item is expired");
				this.cache.remove(key);
				return null;
			}
			return item.data;
		}
	}

	public boolean hasItem(String key) {
		if (logger.isTraceEnabled()) {
			logger.trace("cache size is " + this.cache.size());
			logger.trace("hasItem: cache " + (this.cache.containsKey(key) ? "hit" : "miss"));
			if (this.cache.containsKey(key))
				logger.trace("item is expired: " + this.cache.get(key).isExpired());
		}
		return (this.cache.containsKey(key) && !this.cache.get(key).isExpired());
	}

	public void clear() {
		this.cache.clear();
	}

	public void cleanUp() {
		if (logger.isTraceEnabled()) {
			logger.trace("cache cleanup");
			logger.trace("cache size before cleanup is " + this.cache.size());
		}

		for (String key : this.cache.keySet()) {
			CacheEntry item = this.cache.get(key);
			if (item.isExpired())
				this.cache.remove(key);
		}

		if (logger.isTraceEnabled()) {
			logger.trace("cache size after cleanup is " + this.cache.size());
		}
	}

	public int getSize() {
		return this.cache.size();
	}

	private class CacheEntry {
		private List<? extends Object> data;

		private long expiry;

		public CacheEntry(List<? extends Object> data) {
			this.expiry = System.currentTimeMillis() + ForeignNodesDataCache.ENTRY_TTL;
			this.data = data;
		}

		public boolean isExpired() {
			return this.expiry < System.currentTimeMillis();
		}
	}

}