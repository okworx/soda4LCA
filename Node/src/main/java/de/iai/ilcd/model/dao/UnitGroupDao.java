package de.iai.ilcd.model.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.velocity.tools.generic.ValueParser;

import de.fzk.iai.ilcd.api.app.unitgroup.UnitGroupDataSet;
import de.fzk.iai.ilcd.service.model.IUnitGroupListVO;
import de.fzk.iai.ilcd.service.model.IUnitGroupVO;
import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.datastock.RootDataStock;
import de.iai.ilcd.model.unitgroup.UnitGroup;
import de.iai.ilcd.util.UnmarshalHelper;

/**
 * Data access object for {@link UnitGroup unit groups}
 */
/**
 * @author oli
 *
 */
public class UnitGroupDao extends DataSetDao<UnitGroup, IUnitGroupListVO, IUnitGroupVO> {

	/**
	 * Create the data access object for {@link UnitGroup unit groups}
	 */
	public UnitGroupDao() {
		super( "UnitGroup", UnitGroup.class, IUnitGroupListVO.class, IUnitGroupVO.class, DataSetType.UNITGROUP );
	}

	@Override
	protected String getDataStockField() {
		return "unitGroups";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void preCheckAndPersist( UnitGroup dataSet ) {
		// Nothing to to :)
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void addWhereClausesAndNamedParamesForQueryStringJpql( String typeAlias, ValueParser params, List<String> whereClauses,
			Map<String, Object> whereParamValues ) {
		// nothing to do beside the defaults
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getQueryStringOrderJpql( String typeAlias, String sortString, boolean sortOrder ) {
		if ( "referenceUnit.name".equals( sortString ) ) {
			return typeAlias + ".referenceUnit.name";
		}
		else {
			return super.getQueryStringOrderJpql( typeAlias, sortString, sortOrder );
		}
	}

	/* (non-Javadoc)
	 * @see de.iai.ilcd.model.dao.DataSetDao#getDependencies(de.iai.ilcd.model.common.DataSet, de.iai.ilcd.model.dao.DependenciesMode)
	 */
	@Override
	public Set<DataSet> getDependencies(DataSet dataset, DependenciesMode mode) {
		Set<DataSet> dependencies = new HashSet<DataSet>();
		UnitGroup ug = (UnitGroup) dataset;
		RootDataStock stock = ug.getRootDataStock();

		UnitGroupDataSet xmlDataset = (UnitGroupDataSet) new UnmarshalHelper().unmarshal(ug);

		try {
			addDependencies(xmlDataset.getAdministrativeInformation().getDataEntryBy().getReferenceToDataSetFormat(), stock, dependencies);
		} catch (Exception e) {
		}

		try {
			addDependency(xmlDataset.getAdministrativeInformation().getPublicationAndOwnership().getReferenceToOwnershipOfDataSet(), stock, dependencies);
		} catch (Exception e) {
		}

		return dependencies;
	}

}
