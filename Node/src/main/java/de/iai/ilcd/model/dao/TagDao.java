package de.iai.ilcd.model.dao;

import java.util.List;

import de.iai.ilcd.model.tag.Tag;

public class TagDao extends AbstractLongIdObjectDao<Tag> {

	public TagDao() {
		super(Tag.class.getSimpleName(), Tag.class);
	}
	
	// might need later if remove/merge methods not working
	//private EntityManager em = PersistenceUtil.getEntityManager();
	
	public List<Tag> getTags() {
		return this.getAll();
	}
	
	public Tag getTagByName(String name) {
	    for (Tag t : this.getAll()) {
	        if (t.getName().equals(name)) {
	            return t;
	        }
	    }
	    return null;
	}
	
}
