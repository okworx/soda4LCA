package de.iai.ilcd.model.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.velocity.tools.generic.ValueParser;

import de.fzk.iai.ilcd.api.app.flowproperty.FlowPropertyDataSet;
import de.fzk.iai.ilcd.service.model.IFlowPropertyListVO;
import de.fzk.iai.ilcd.service.model.IFlowPropertyVO;
import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.datastock.RootDataStock;
import de.iai.ilcd.model.flowproperty.FlowProperty;
import de.iai.ilcd.model.unitgroup.UnitGroup;
import de.iai.ilcd.util.UnmarshalHelper;

/**
 * Data access object for {@link FlowProperty flow properties}
 */
public class FlowPropertyDao extends DataSetDao<FlowProperty, IFlowPropertyListVO, IFlowPropertyVO> {

	/**
	 * Create the data access object for {@link FlowProperty flow properties}
	 */
	public FlowPropertyDao() {
		super( "FlowProperty", FlowProperty.class, IFlowPropertyListVO.class, IFlowPropertyVO.class, DataSetType.FLOWPROPERTY );
	}

	@Override
	protected String getDataStockField() {
		return "flowProperties";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void preCheckAndPersist( FlowProperty dataSet ) {
		this.attachUnitGroup( dataSet );
	}

	/**
	 * Get flow property by UUID. Overrides {@link DataSetDao} implementation because
	 * {@link #attachUnitGroup(FlowProperty)} is called after loading.
	 */
	@Override
	public FlowProperty getByUuid( String uuid ) {
		FlowProperty fp = super.getByUuid( uuid );
		if ( fp != null ) {
			this.attachUnitGroup( fp );
		}
		return fp;
	}

	private void attachUnitGroup( FlowProperty flowprop ) {
		UnitGroupDao unitGroupDao = new UnitGroupDao();
		if ( flowprop != null && flowprop.getReferenceToUnitGroup() != null ) {
			String uuid = flowprop.getReferenceToUnitGroup().getUuid().getUuid();
			UnitGroup unitGroup = unitGroupDao.getByUuid( uuid );
			flowprop.setUnitGroup( unitGroup );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getQueryStringOrderJpql( String typeAlias, String sortString, boolean sortOrder ) {
		if ( "unitGroupName.value".equals( sortString ) ) {
			return typeAlias + ".unitGroup.name.value";
		}
		else if ( "defaultUnit".equals( sortString ) ) {
			return typeAlias + ".unitGroup.referenceUnit.name";
		}
		else {
			return super.getQueryStringOrderJpql( typeAlias, sortString, sortOrder );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void addWhereClausesAndNamedParamesForQueryStringJpql( String typeAlias, ValueParser params, List<String> whereClauses,
			Map<String, Object> whereParamValues ) {
		// nothing to do beside the defaults
	}

	/* (non-Javadoc)
	 * @see de.iai.ilcd.model.dao.DataSetDao#getDependencies(de.iai.ilcd.model.common.DataSet, de.iai.ilcd.model.dao.DependenciesMode)
	 */
	@Override
	public Set<DataSet> getDependencies(DataSet dataset, DependenciesMode mode) {
		Set<DataSet> dependencies = new HashSet<DataSet>();
		FlowProperty fp = (FlowProperty) dataset;
		RootDataStock stock = fp.getRootDataStock();

		// unit group
		addDependency(fp.getUnitGroup(), fp.getReferenceToUnitGroup(), stock, dependencies);

		FlowPropertyDataSet xmlDataset = (FlowPropertyDataSet) new UnmarshalHelper().unmarshal(fp);

		try {
			addDependencies(xmlDataset.getAdministrativeInformation().getDataEntryBy().getReferenceToDataSetFormat(),
					stock, dependencies);
		} catch (Exception e) {
		}

		try {
			addDependency(xmlDataset.getAdministrativeInformation().getPublicationAndOwnership()
					.getReferenceToOwnershipOfDataSet(), stock, dependencies);
		} catch (Exception e) {
		}

		try {
			addDependency(xmlDataset.getFlowPropertiesInformation().getQuantitativeReference()
					.getReferenceToReferenceUnitGroup(), stock, dependencies);

		} catch (Exception e) {
		}

		return dependencies;
	}

}
