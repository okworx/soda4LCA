package de.iai.ilcd.service.job;

import java.util.Date;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.listeners.JobListenerSupport;

import de.iai.ilcd.model.dao.MergeException;
import de.iai.ilcd.service.JobService;

/**
 * The job listener. This listener updates with begin, veto and completion of a
 * job execution the given {@link JobService}.
 * 
 * @author sarai
 *
 */
public class JobListener extends JobListenerSupport {

	/*
	 * The logger
	 */
	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());

	/*
	 * The name of this listener
	 */
	private String name = UUID.randomUUID().toString();

	/*
	 * The given job service
	 */
	private JobService jobService;

	/**
	 * Initializes the job listener with given {@link JobService}.
	 * 
	 * @param service
	 */
	public JobListener(JobService service) {
		this.jobService = service;
	}
	
	/**
	 * Initializes the job listener with given {@link JobService}.
	 * 
	 * @param service
	 * @param name
	 */
	public JobListener(JobService service, String name) {
		this.jobService = service;
		this.name = name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void jobToBeExecuted(JobExecutionContext context) {
		updateJob(context, null, JobState.RUNNING);
	}

	/**
	 * {@inheritDoc}}
	 */
	@Override
	public void jobExecutionVetoed(JobExecutionContext context) {
		if (log.isDebugEnabled()) {
			log.debug(context.getJobDetail().getDescription() + "is vetoed.");
		}
		updateJob(context, null, JobState.ERROR);
	}

	/**
	 * {@inheritDoc}}
	 */
	@Override
	public void jobWasExecuted(JobExecutionContext context, JobExecutionException exception) {
		Date completionDate = new Date(context.getFireTime().getTime() + context.getJobRunTime());

		if (!(exception == null || exception.getMessage().equals(""))) {
			updateJob(context, completionDate, JobState.ERROR);
			jobService.informError(context.getJobDetail().getDescription());
		} else {
			updateJob(context, completionDate, JobState.COMPLETE);
			jobService.informComplete(context.getJobDetail().getDescription());
		}
	}

	/**
	 * Gets the {@link JobService}.
	 * 
	 * @return The job service
	 */
	public JobService getJobService() {
		return jobService;
	}

	/**
	 * Sets the {@link JobService} with given data.
	 * 
	 * @param jobService
	 *            The job service
	 */
	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	/**
	 * Updates given job.
	 * 
	 * @param context
	 *            The job execution context of given job
	 * @param completionDate
	 *            The completion date of given job
	 */
	private void updateJob(JobExecutionContext context, Date completionDate, JobState jobState) {
		try {
			jobService.updateJob(context, completionDate, jobState);
		} catch (MergeException e) {
			log.warn("Job could not be updated!");
			if (log.isDebugEnabled()) {
				e.printStackTrace();
			}
		}
	}

}
