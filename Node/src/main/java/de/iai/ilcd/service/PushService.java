package de.iai.ilcd.service;

import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;
import org.springframework.stereotype.Service;

import de.fzk.iai.ilcd.service.client.ILCDServiceClientException;
import de.fzk.iai.ilcd.service.client.impl.ILCDNetworkClient;
import de.iai.ilcd.model.common.PushConfig;
import de.iai.ilcd.model.dao.MergeException;
import de.iai.ilcd.model.dao.PersistException;
import de.iai.ilcd.model.dao.PushConfigDao;
import de.iai.ilcd.model.security.User;
import de.iai.ilcd.rest.util.NotAuthorizedException;
import de.iai.ilcd.service.exception.JobNotScheduledException;
import de.iai.ilcd.service.job.JobState;
import de.iai.ilcd.service.job.JobType;
import de.iai.ilcd.service.job.PushJob;

/**
 * The service class for pushing data stocks.
 * 
 * @author sarai
 *
 */
@Service("pushService")
public class PushService extends JobService {

	public PushService() throws SchedulerException {
		super();
	}

	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());
	
	private static final String REST_SERVLET_PREFIX = "resource/";

	/**
	 * Pushes data from given push configuration to stock in target node given
	 * by push configuration. Needs password of target node for authorization.
	 * 
	 * @param pushConfig
	 *            The push configuration containing all relevant information for
	 *            pushig data sets
	 * @param password
	 *            The password for authorization of target node
	 * @throws SchedulerException
	 *             If scheduler cannot be instantiated or job is not scheduled
	 *             correctly
	 * @throws PersistException
	 * @throws MergeException
	 * @throws JobNotScheduledException
	 * @throws ILCDServiceClientException
	 * @throws NotAuthorizedException
	 */
	public final void pushDataStock(PushConfig pushConfig, String password, User user)
			throws SchedulerException, PersistException, MergeException, JobNotScheduledException,
			ILCDServiceClientException, NotAuthorizedException {

		String baseUrl = new String(pushConfig.getTarget().getTargetURL());
		if ( !pushConfig.getTarget().getTargetURL().endsWith( "/" ) ) {
			baseUrl = baseUrl + "/";
		}
		if (!baseUrl.endsWith(REST_SERVLET_PREFIX) )
			baseUrl += REST_SERVLET_PREFIX;
		ILCDNetworkClient client = new ILCDNetworkClient(baseUrl,
				pushConfig.getTarget().getLogin(), password);
		if (!client.getAuthenticationStatus().isAuthenticated()) {
			log.warn("Is not authenticated.");
			throw new NotAuthorizedException();
		}

		JobDataMap jobDataMap = new JobDataMap();
		log.debug("Getting Job description");

		StringBuilder jobDescription = new StringBuilder(pushConfig.getSource().getName()).append(" to ")
				.append(pushConfig.getTarget().getTargetDsName()).append(" in ")
				.append(pushConfig.getTarget().getTargetID());
		jobDataMap.put("pushConfig", pushConfig);
		jobDataMap.put("type", JobType.PUSH);
		jobDataMap.put("client", client);
		JobDetail job = JobBuilder.newJob(PushJob.class).withDescription(jobDescription.toString())
				.usingJobData(jobDataMap).build();
		log.debug("Trying to queue push job.");
		super.queueJob(job, user, JobType.PUSH);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateJob(JobExecutionContext context, Date jobCompletionTime, JobState jobState)
			throws MergeException {
		super.updateJob(context, jobCompletionTime, jobState);
		log.debug("updating push job meta data");
		PushConfigDao pushConfigDao = new PushConfigDao();
		JobDataMap jobDataMap = context.getMergedJobDataMap();
		PushConfig pushConfig = (PushConfig) jobDataMap.get("pushConfig");
		if (pushConfig != null) {
			pushConfig.setLastPushDate(context.getFireTime());
			pushConfig.setLastJobState(jobState);
		}
		pushConfigDao.merge(pushConfig);
	}

}
