package de.iai.ilcd.service.glad;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;

import de.iai.ilcd.configuration.ConfigurationService;
import de.iai.ilcd.delegate.RestServiceBD;
import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.rest.util.InvalidGLADUrlException;
import eu.europa.ec.jrc.lca.commons.rest.dto.DataSetRegistrationResult;
import eu.europa.ec.jrc.lca.commons.service.exceptions.AuthenticationException;
import eu.europa.ec.jrc.lca.commons.service.exceptions.NodeIllegalStatusException;
import eu.europa.ec.jrc.lca.commons.service.exceptions.RestWSUnknownException;

public class GLADRestServiceBD extends RestServiceBD {


	private final Logger log = LoggerFactory.getLogger(GLADRestServiceBD.class);

	private static final String REST_PATH = "api/v1";
	private static final String PATH_PREFIX = "/search/index";
	private static final String AUTH_HEADER_NAME = "Authorization";
	private static final String AUTH_BEARER_PREFIX = "Bearer ";

	private GLADRestServiceBD(String serviceBasicUrl) {
		super(serviceBasicUrl);
	}

	static GLADRestServiceBD getInstance() {
		String urlPath = ConfigurationService.INSTANCE.getGladURL();
		if (!urlPath.endsWith("/")) {
			StringBuffer buffer = new StringBuffer(urlPath);
			buffer.append("/");
			urlPath = buffer.toString();
		}
		return new GLADRestServiceBD(urlPath + REST_PATH);
	}

	/**
	 * Registers a given data set with given information whether data set is an
	 * updated version of an existing data set.
	 * 
	 * @param dataset
	 *            The data set that shall be registered
	 * @param update
	 *            The flag indication whether data set already exists and shall
	 *            be updated in GLAD
	 * @return A Result containing information whether data set could be
	 *         successfully registered, not registered since either data set
	 *         already exists or data set is invalid
	 * @throws NodeIllegalStatusException
	 * @throws AuthenticationException
	 * @throws RestWSUnknownException
	 * @throws InvalidGLADUrlException 
	 */
	public DataSetRegistrationResult registerDataSet(DataSet dataset, boolean update)
			throws NodeIllegalStatusException, AuthenticationException, RestWSUnknownException, InvalidGLADUrlException {
		
		try {
			if (log.isTraceEnabled()) {
				log.trace("GLAD API key: " + ConfigurationService.INSTANCE.getGladAPIKey());
			}
			ClientResponse response = getResource("/search?query=foo").header(AUTH_HEADER_NAME, AUTH_BEARER_PREFIX + ConfigurationService.INSTANCE.getGladAPIKey()).get(ClientResponse.class);
			log.debug("response: " + response.getStatus());
			if (response.getStatus() != 200) {
				log.error("invalid url, status: " + response.getStatus());
				throw new InvalidGLADUrlException();
			}
		} catch (ClientHandlerException che) {
			throw new InvalidGLADUrlException(che.getMessage());
		}
		
		try {

			String refId = dataset.getUuid().getUuid();
			
			if (log.isDebugEnabled())
				log.debug("registering dataset " + refId + ", update=" + update);
			
			WebResource wr;
			if (update)
				wr = getResource(PATH_PREFIX + "/" + refId);
			else
				wr = getResource(PATH_PREFIX);
			GLADMetaDataUtil datasetMdUtil = new GLADMetaDataUtil();
			GLADMetaData datasetMd = new GLADMetaData();
			datasetMdUtil.setGLADMetaData(datasetMd);
			datasetMd = datasetMdUtil.convertToGLADMetaData(dataset);
			ClientResponse cr;
			Builder header = wr.header(AUTH_HEADER_NAME, AUTH_BEARER_PREFIX + ConfigurationService.INSTANCE.getGladAPIKey());
			
			if (update == false) {
				cr = header.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, datasetMd);
				if (cr.getStatus() == 409) {
					return DataSetRegistrationResult.REJECTED_NO_DIFFERENCE;
				}
			} else {
				cr = header.type(MediaType.APPLICATION_JSON).put(ClientResponse.class, datasetMd);
			}
			if (log.isDebugEnabled())
				log.debug("finished with status code " + cr.getStatus());
			
			if (cr.getStatus() == 422) {
				return DataSetRegistrationResult.REJECTED_COMPLIANCE;
			} else if (cr.getStatus() == 201) {
				return DataSetRegistrationResult.ACCEPTED_PENDING;
			}
			log.error("status code: " + cr.getStatus());
			return DataSetRegistrationResult.ERROR;
		} catch (ClientHandlerException ex) {
			throw new RestWSUnknownException(ex.getMessage());
		}
	}

	/**
	 * Deregisters data set with given refId/UUID
	 * 
	 * @param refId
	 *            The refID/UUID of data set that shall be deregistered from
	 *            GLAD
	 * @throws AuthenticationException
	 * @throws RestWSUnknownException
	 */
	public void deregisterDataSet(String refId) throws AuthenticationException, RestWSUnknownException {
		try {
			WebResource wr = getResource(PATH_PREFIX + "/" + refId);
			ClientResponse cr = wr.header(AUTH_HEADER_NAME, AUTH_BEARER_PREFIX + ConfigurationService.INSTANCE.getGladAPIKey())
					.type(MediaType.APPLICATION_JSON).delete(ClientResponse.class);
			if (cr.getStatus() == 401) {
				throw new AuthenticationException();
			}
		} catch (ClientHandlerException ex) {
			throw new RestWSUnknownException(ex.getMessage());
		}
	}
}
