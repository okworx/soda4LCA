package de.iai.ilcd.service.glad;

import javax.persistence.metamodel.SingularAttribute;

import de.iai.ilcd.model.common.DataSetVersion;

@javax.persistence.metamodel.StaticMetamodel(de.iai.ilcd.service.glad.GLADRegistrationData.class)
public class GLADRegistrationData_ {

	public static volatile SingularAttribute<GLADRegistrationData, Long> id;

	public static volatile SingularAttribute<GLADRegistrationData, String> uuid;

	public static volatile SingularAttribute<GLADRegistrationData, DataSetVersion> version;

}
