package de.iai.ilcd.service.job;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.common.JobMetaData;
import de.iai.ilcd.model.dao.DependenciesMode;
import de.iai.ilcd.util.DependenciesOptions;
import de.iai.ilcd.util.sort.DataSetComparator;

@Service("jobLoggerSrevice")
public class AssignRemoveJobLoggerService {

	static final Logger log = LoggerFactory.getLogger(AssignRemoveJobLoggerService.class);

	private enum LogLevel {
		SUCCESS, ERROR, INFO
	}

	private DependenciesOptions dependenciesOptions = new DependenciesOptions();

	/**
	 * The mapping from uuid and version of each processed data set to flag whether
	 * processed is true
	 **/
	Map<String, Boolean> processedDependencies = new HashMap<String, Boolean>();

	/**
	 * The unique ID of this job
	 **/
	StringBuilder jobId;

	/**
	 * The job metadata of this job
	 **/
	JobMetaData jmd;

	/**
	 * The log of all successes in job execution
	 **/
	StringBuilder successesLog;

	/**
	 * The log of all infos during job execution
	 **/
	StringBuilder infosLog;

	/**
	 * The log of all errors during job execution
	 **/
	StringBuilder errorsLog;

	Map<String, DataSetInfo> dataSetInfos = new HashMap<String, DataSetInfo>();

	/**
	 * The mapping from uuid and version of each proccess to list of its
	 * dependencies
	 **/
	Map<String, List<String>> processDependencies = new HashMap<String, List<String>>();

	private void setLog(boolean exists, String response, DataSet dataSet, String operationName) {
		LogLevel logLvl;
		String logLevel = "";
		String suffix = "";
		boolean successful = true;
		jmd.setJobRunTime(System.currentTimeMillis() - jmd.getJobFireTime().getTime());
		StringBuilder sBuilder = new StringBuilder(" Dependencies status is:\n");
		List<String> dependencies = processDependencies
				.get(dataSet.getUuid().getUUID() + "," + dataSet.getVersion().getVersionString());
		if (dependencies != null && !dependencies.isEmpty()) {
			for (String dependency : dependencies) {
				if (processedDependencies.get(dependency) != null) {
					String status;
					String depSuffix;
					log.debug("dependency is: " + dependency);
					boolean dependencySuccessful = processedDependencies.get(dependency);
					DataSetInfo dsInfo = dataSetInfos.get(dependency);
					if (!dependencySuccessful) {
						successful = false;
						status = "ERROR ";
						depSuffix = "could not be " + operationName;
					} else {
						status = "SUCCESS";
						depSuffix = "was " + operationName + "successfully";
					}
					sBuilder.append("&nbsp;&nbsp;&nbsp;").append(status).append(dsInfo.getType()).append(" dataset ")
							.append(dsInfo.getName()).append(" ( uuid: ").append(dsInfo.getUuid()).append(") ")
							.append(depSuffix).append("\n");
				}
			}
		}

		if (!exists && response == "OK") {
			if (successful) {
				logLvl = LogLevel.SUCCESS;
				logLevel = " SUCCESS ";
				if (this.dependenciesOptions.getDependenciesOption() == DependenciesMode.ALL)
					suffix = "was successfully " + operationName + " including all dependencies.";
				else {
					suffix = "was successfully " + operationName + " .";
				}
				jmd.addSuccessesCount();
			} else {
				logLvl = LogLevel.ERROR;
				logLevel = " ERROR ";
				suffix = new StringBuilder(
						"was successfully " + operationName + ", but at least one dependency wasn't.")
								.append(sBuilder.toString()).toString();
				jmd.addErrorsCount();
			}
		} else if (exists) {
			logLvl = LogLevel.INFO;
			logLevel = " INFO ";
			jmd.addInfosCount();
			if(operationName == "attached")
				suffix = "was not " + operationName + " since it already exists in target.";
			if(operationName == "detached")
				suffix = "was not " + operationName + " since it doesn't exist in target.";
		} else {
			logLvl = LogLevel.ERROR;
			logLevel = " ERROR ";
			suffix = new StringBuilder("could be not " + operationName + " due to an error.")
					.append(sBuilder.toString()).toString();
			jmd.addErrorsCount();
			if (log.isDebugEnabled()) {
				log.debug("response status: " + response);
			}
		}

		Date date = new Date();
		StringBuilder sb = new StringBuilder(date.toString()).append(logLevel)
				.append("Data set ( " + dataSet.getName().getValue() + " ) ").append(suffix);

		try {
			switch (logLvl) {
			case SUCCESS:
				jmd.appendLog(sb.toString() + "\n");
				successesLog.append(sb.toString() + "\n");
				break;
			case INFO:
				jmd.appendLog(sb.toString() + "\n");
				infosLog.append(sb.toString() + "\n");
				break;
			case ERROR:
				jmd.appendLog(sb.toString() + "\n");
				errorsLog.append(sb.toString() + "\n");
				break;
			}
		} catch (Exception e) {
			System.err.println("Heap error");
		}

		if (log.isTraceEnabled()) {
			log.trace("errors count: " + jmd.getErrorsCount());
			log.trace("infos count: " + jmd.getInfosCount());
			log.trace("successes count: " + jmd.getSuccessesCount());
		}
	}

	/**
	 * This method fills processDependencies map with the dependencies and their
	 * info.
	 */
	public void fillingProcessDependenciesMap(DataSet dataSet, Set<DataSet> resultDependencies, String operationName)
			throws IOException {
		List<DataSet> dependencyList = new ArrayList<DataSet>();

		if (dataSet != null) {
			if (resultDependencies != null) {
				dependencyList.addAll(resultDependencies);
			}
			List<String> dependencies = new ArrayList<String>();
			for (DataSet resultDependency : resultDependencies) {
				String uuidVersion = resultDependency.getUuid().getUuid() + ","
						+ resultDependency.getVersion().getVersionString();
				dependencies.add(uuidVersion);
				DataSetInfo dsInfo = new DataSetInfo();
				dsInfo.setName(resultDependency.getName().getValue());
				dsInfo.setUuid(resultDependency.getUuid().getUuid());
				dsInfo.setVersion(resultDependency.getVersion().getVersionString());
				dsInfo.setType(resultDependency.getDataSetType().getValue());
				dataSetInfos.put(uuidVersion, dsInfo);
			}
			processDependencies.put(dataSet.getUuid().getUuid() + "," + dataSet.getVersion().getVersionString(),
					dependencies);
		}

		dependencyList.remove(dataSet);

		Collections.sort(dependencyList, new DataSetComparator());
		HashSet<DataSet> dependencySet = new HashSet<DataSet>(dependencyList);
		if (log.isDebugEnabled()) {
			log.debug("Size of dependency list is: " + dependencyList.size());
		}

		if (dependencyList != null) {
			if (log.isTraceEnabled()) {
				log.trace("Processing dependencies.");
			}

			for (DataSet ds : dependencySet) {
				if (log.isDebugEnabled()) {
					log.debug("current data set is of type: " + ds.getDataSetType().getValue());
				}
				this.processDataSet(ds, false, true, operationName);
				if (log.isDebugEnabled())
					log.debug("successfully pushed dependency with uuid " + ds.getUuidAsString());
			}
		}
	}

	public void processDataSet(DataSet dataSet, boolean dsExists, boolean isDependency, String operationName)
			throws IOException {
		log.debug("Begin of assigning and removing data set");
		String response = "OK";

		String uuidVersion = dataSet.getUuid().getUuid() + "," + dataSet.getVersion().getVersionString();
		if (isDependency && this.processedDependencies.get(uuidVersion) == null) {
			boolean isProcessed = true;
			processedDependencies.put(uuidVersion, isProcessed);
		}

		if (!isDependency)
			setLog(dsExists, response, dataSet, operationName);
	}

	/**
	 * local class containing all information relevant for log in JobMetaData
	 */
	private class DataSetInfo {
		private String uuid;
		private String name;
		private String version;

		private String type;

		public String getUuid() {
			return uuid;
		}

		public void setUuid(String uuid) {
			this.uuid = uuid;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getVersion() {
			return this.version;
		}

		public void setVersion(String version) {
			this.version = version;
		}

		public String getType() {
			return this.type;
		}

		public void setType(String type) {
			this.type = type;
		}
	}

	public void setMetaData(org.apache.log4j.Logger log, JobMetaData jmd, StringBuilder successesLog,
			StringBuilder infosLog, StringBuilder errorsLog, StringBuilder jobId) {
		this.jmd = jmd;
		this.successesLog = successesLog;
		this.infosLog = infosLog;
		this.errorsLog = errorsLog;
		this.jobId = jobId;
	}

	public DependenciesOptions getDependenciesOptions() {
		return dependenciesOptions;
	}
}
