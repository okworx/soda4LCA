package de.iai.ilcd.service.job;

/**
 * This enum represents the states a job can have.
 * 
 * @author sarai
 *
 */
public enum JobState {
	WAITING("waiting"), RUNNING("running"), ERROR("error"), COMPLETE("complete"), NONE("none");

	private String value;

	/**
	 * Initializes job state with given value
	 * 
	 * @param value
	 *            The value of job state
	 */
	JobState(String value) {
		this.value = value;
	}

	/**
	 * Gets job state from its value name.
	 * 
	 * @param value
	 *            the value of job state
	 * @return
	 */
	public static JobState fromValue(String value) {
		for (JobState enumValue : JobState.values()) {
			if (enumValue.getValue().equals(value))
				return enumValue;
		}
		return null;
	}

	/**
	 * Gets value of job state.
	 * 
	 * @return Value of job state
	 */
	public String getValue() {
		return value;
	}

}
