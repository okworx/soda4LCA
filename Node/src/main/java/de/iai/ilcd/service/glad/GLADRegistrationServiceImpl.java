package de.iai.ilcd.service.glad;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.iai.ilcd.model.process.Process;
import de.iai.ilcd.rest.util.InvalidGLADUrlException;
import eu.europa.ec.jrc.lca.commons.dao.SearchParameters;
import eu.europa.ec.jrc.lca.commons.rest.dto.DataSetRegistrationResult;
import eu.europa.ec.jrc.lca.commons.service.exceptions.AuthenticationException;
import eu.europa.ec.jrc.lca.commons.service.exceptions.NodeIllegalStatusException;
import eu.europa.ec.jrc.lca.commons.service.exceptions.RestWSUnknownException;

/**
 * The implementation of the GLAD registration service object.
 * 
 * @author sarai
 *
 */
@Service("gladRegistrationService")
public class GLADRegistrationServiceImpl implements GLADRegistrationService {

	private final Logger log = LoggerFactory.getLogger(GLADRegistrationServiceImpl.class);

	@Autowired
	private GLADRegistrationDataDao dao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GLADRegistrationData> getRegistered() {
		return dao.getRegistered();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DataSetRegistrationResult> register(List<Process> processes)
			throws RestWSUnknownException, AuthenticationException, NodeIllegalStatusException, InvalidGLADUrlException {
		List<DataSetRegistrationResult> resultList = new ArrayList<DataSetRegistrationResult>();
		for (Process process : processes) {
			boolean update;

			GLADRegistrationData dataSetGlad = new GLADRegistrationData();
			try {
				dataSetGlad = dao.findByUUID(process.getUuid().getUuid());

				if (dataSetGlad == null) {
					update = false;
				} else {
					if (dataSetGlad.getVersion().compareTo(process.getVersion()) >= 0) {
						resultList.add(DataSetRegistrationResult.REJECTED_NO_DIFFERENCE);
						continue;
					} else {
						update = true;
					}
				}
			} catch (NoResultException nre) {
				update = false;
			}
			DataSetRegistrationResult result = GLADRestServiceBD.getInstance().registerDataSet(process, update);
			resultList.add(result);
			if (result == DataSetRegistrationResult.ACCEPTED_PENDING)
				logDataSetRegistrationInfrmation(process, update);

		}
		return resultList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deregister(List<Process> processes) {
		for (Process process : processes) {
			try {
				GLADRegistrationData regData = dao.findByUUID(process.getUuid().getUuid());
				if (regData != null) {
					GLADRestServiceBD.getInstance().deregisterDataSet(process.getUuid().getUuid());

					dao.remove(regData.getId());
				}
			} catch (AuthenticationException e) {
				e.printStackTrace();
			} catch (RestWSUnknownException e) {
				e.printStackTrace();
			}

		}
	}

	/**
	 * Persists GLAD registration data of given process into database. Replaces
	 * existing process with older version if such a process already exists.
	 * 
	 * @param process
	 *            The process of which GLAD registration data shall persist
	 * @param update
	 *            The flag indicating if process is an updated version of
	 *            existing entry
	 */
	private void logDataSetRegistrationInfrmation(Process process, boolean update) {
		if (update) {
			GLADRegistrationData dsg = dao.findByUUID(process.getUuid().getUuid());
			dao.remove(dsg.getId());
		}
		GLADRegistrationData dsrd = new GLADRegistrationData();
		dsrd.setVersion(process.getVersion());
		dsrd.setUuid(process.getUuidAsString());

		dao.saveOrUpdate(dsrd);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GLADRegistrationData> getListOfRegistrations( Process process ) {
		return dao.getListOfRegistrations( process );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GLADRegistrationData> loadLazy(SearchParameters sp) {
		return dao.find(sp);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long count(SearchParameters sp) {
		return dao.count(sp);
	}

}
