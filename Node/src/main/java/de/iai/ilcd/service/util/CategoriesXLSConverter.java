package de.iai.ilcd.service.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fzk.iai.ilcd.api.app.categories.Categories;
import de.fzk.iai.ilcd.api.app.categories.Category;
import de.fzk.iai.ilcd.api.app.categories.CategorySystem;
import de.fzk.iai.ilcd.api.binding.generated.categories.CategoryType;
import de.fzk.iai.ilcd.service.client.impl.vo.epd.ProcessSubType;
import de.iai.ilcd.configuration.ConfigurationService;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class CategoriesXLSConverter implements Serializable {

	private static final long serialVersionUID = 5612516188877667998L;

	public static Logger logger = LoggerFactory.getLogger(CategoriesXLSConverter.class);

	private ResourceBundle resourceBundle;

	public void generateXLS(CategorySystem cs, String language, boolean showCounts, Map<String, Map<String, Integer>> counts, String stockName, ResourceBundle resourceBundle, OutputStream os) throws IOException {

		this.resourceBundle = resourceBundle;

		WritableWorkbook wb = Workbook.createWorkbook(os);

		String name = cs.getName() + ("de".equalsIgnoreCase(language) ? "-" : " ")
				+ resourceBundle.getString("admin.categories.categorydefinitions.categories");
		WritableSheet sheetData = wb.createSheet(name, 0);
		WritableSheet sheetDataPretty = wb.createSheet(name + resourceBundle.getString("admin.categories.categorydefinitions.categories.pretty"), 1);
		WritableSheet sheetMetaInfo = wb.createSheet("Info", 2);

		Date date = Calendar.getInstance().getTime();
		SimpleDateFormat dateFormatTimeStamp = new SimpleDateFormat("dd.MM.yyyy 'at' HH:mm:ss");
		String newDate = dateFormatTimeStamp.format(date);

		try {
			sheetMetaInfo.addCell(new Label(0, 0, "generated on"));
			sheetMetaInfo.addCell(new Label(1, 0, newDate));

			sheetMetaInfo.addCell(new Label(0, 1, "Node ID"));
			sheetMetaInfo.addCell(new Label(1, 1, ConfigurationService.INSTANCE.getNodeId() + " (" + ConfigurationService.INSTANCE.getNodeName() + ")"));

			sheetMetaInfo.addCell(new Label(0, 2, "soda4LCA"));
			sheetMetaInfo.addCell(new Label(1, 2, ConfigurationService.INSTANCE.getVersionTag()));

			if (StringUtils.isNotBlank(stockName)) {
				sheetMetaInfo.addCell(new Label(0, 3, "data stock"));
				sheetMetaInfo.addCell(new Label(1, 3, stockName));
			}
			
			writeHeader(sheetData, true, showCounts);
			writeHeader(sheetDataPretty, false, showCounts);

		} catch (RowsExceededException e) {
			logger.error("Error generating XLS file", e);
		} catch (WriteException e) {
			logger.error("Error writing XLS file", e);
		}

		Categories processCategories = (Categories) cs.getCategories().get(0);

		writeCategories(processCategories.getCategory(), 0, 0, sheetData, counts, false);

		writeCategories(processCategories.getCategory(), 0, 0, sheetDataPretty, counts, true);

		format(sheetMetaInfo);
		format(sheetData);
		format(sheetDataPretty);

		wb.write();

		try {
			wb.close();
		} catch (WriteException e) {
			logger.error("Error generating XLS file", e);
		}
	}

	private void format(WritableSheet sheet) {
		int maxWidth = 0;

		for (int i = 0; i < sheet.getColumns(); i++) {
			for (int j = 0; j < sheet.getRows(); j++) {
				WritableCell cell = (WritableCell) sheet.getCell(i, j);
				int len = cell.getContents().length();
				if (len > maxWidth) {
					maxWidth = len + 1;
					sheet.setColumnView(i, maxWidth);
				}
			}
		}

	}

	private int writeCategories(List<CategoryType> cats, int level, int rownum, WritableSheet sheet, Map<String, Map<String, Integer>> counts, boolean pretty) {

		for (CategoryType catType : cats) {

			rownum++;

			Category cat = (Category) catType;

			Integer count = 0;
			
			try {
				WritableFont font = new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD);
				WritableCellFormat cf = new WritableCellFormat(font);

				int i = 0;
				if (!pretty) {
					WritableCell cell0 = new Number(i, rownum, level + 1, cf);
					sheet.addCell(cell0);
					i++;
				}
				String prettyPrefix = StringUtils.repeat("  ", level);
				WritableCell cell1 = new Label(i, rownum, (pretty ? prettyPrefix : "") + cat.getId(), cf);
				i++;
				WritableCell cell2 = new Label(i, rownum, (pretty ? prettyPrefix : "") + cat.getName(), cf);

				sheet.addCell(cell1);
				sheet.addCell(cell2);

				if (counts != null) {
					i++;
					count = counts.get("ALL").get(cat.getId()); 
					if (count != null) {
						WritableCell cell3 = new Number(i, rownum, count, cf);
						sheet.addCell(cell3);
					}
					for (ProcessSubType subType : ProcessSubType.values()) {
						count = counts.get(subType.getValue()).get(cat.getId()); 
						i++;
						if (count != null) {
							WritableCell cellc = new Number(i, rownum, count, cf);
							sheet.addCell(cellc);
						}
						
					}
						
				}
				
			} catch (WriteException e) {
				logger.error("Error generating XLS", e);
			}

			if (logger.isDebugEnabled())
				logger.debug(rownum + " " + level + " " + cat.getId() + " " + cat.getName() + "    subcats:" + (cat.getCategory().size() != 0) + " " + (count != null ? count.toString() : ""));

			if (cat.getCategory().size() != 0) {
				rownum = writeCategories(cat.getCategory(), level + 1, rownum, sheet, counts, pretty);
			}
		}
		return rownum;
	}

	private void writeHeader(WritableSheet sheet, boolean showLevel, boolean count) throws RowsExceededException, WriteException {

		WritableFont boldFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
		WritableCellFormat cfBold = new WritableCellFormat(boldFont);

		int i = 0;

		if (showLevel) {
			sheet.addCell(new Label(i, 0, this.resourceBundle.getString("admin.categories.categorydefinitions.categories.level"), cfBold));
			i++;
		}
		sheet.addCell(new Label(i, 0, this.resourceBundle.getString("admin.categories.categorydefinitions.categories.id"), cfBold));
		i++;
		sheet.addCell(new Label(i, 0, this.resourceBundle.getString("admin.categories.categorydefinitions.categories.name"), cfBold));
		i++;
		
		if (count) {
			sheet.addCell(new Label(i, 0, this.resourceBundle.getString("admin.categories.categorydefinitions.categories.count"), cfBold));
			i++;

			for (ProcessSubType subType : ProcessSubType.values()) {
				sheet.addCell(new Label(i, 0, subType.getValue().replace(" dataset", ""), cfBold));
				i++;
			}
		}
	}
}
