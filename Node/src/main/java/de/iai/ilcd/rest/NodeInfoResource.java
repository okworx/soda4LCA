package de.iai.ilcd.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import de.fzk.iai.ilcd.service.client.impl.vo.nodeinfo.NodeInfo;
import de.iai.ilcd.configuration.ConfigurationService;

/**
 * REST Web Service
 * 
 * @author oliver.kusche
 */
@Component
@Path( "nodeinfo" )
public class NodeInfoResource {

	private static final Logger logger = LoggerFactory.getLogger( NodeInfoResource.class );

	/** Creates a new instance of NodeInfoResource */
	public NodeInfoResource() {
	}

	@GET
	@Produces( "application/xml" )
	public NodeInfo status() {
		logger.info( "nodeinfo" );

		return ConfigurationService.INSTANCE.getNodeInfo();

	}

}
